using System;
using System.IO;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;

namespace AttaboysGames.TemplateCreatorScriptMover.Editor
{
	public class ScriptsMoverTool
	{
		private static ListRequest _request;
		private static bool _isFound;
		private static string _path;

		[MenuItem("Tools/CreateTemplate/4: Move scripts", false, 4)]
		private static void MoveScripts()
		{
			_request = Client.List(true);
			EditorApplication.update += Update;
			
			while(!_isFound)
				return;

			var finalPath = Path.Combine(_path, "Scripts");
			var packageRuntimeFiles = Directory.GetFiles(finalPath,"*.*", SearchOption.AllDirectories);
			var currentDirectory = Environment.CurrentDirectory;
			var targetPath = currentDirectory + "\\Assets\\Scripts";
			Directory.CreateDirectory(targetPath);

			foreach (var file in packageRuntimeFiles)
			{
				var relativePath = Path.GetRelativePath(finalPath, file);
				var destination = Path.Combine(targetPath, relativePath);
			
				var destinationDirectory = Path.GetDirectoryName(destination);
				Directory.CreateDirectory(destinationDirectory);
			
				File.Copy(file, destination);
			}
			
			Debug.Log("All script imported.");
		}

		private static void Update()
		{
			if (_request.IsCompleted)
			{
				switch (_request.Status)
				{
					case StatusCode.Success:
					{
						foreach (var package in _request.Result)
						{
							if (package.name != "com.dikiyhimik.templatecreatorscriptmover")
								continue;

							_path = package.assetPath;
							_isFound = true;
							EditorApplication.update -= Update;
							return;
						}

						break;
					}
					case >= StatusCode.Failure:
						Debug.LogError("Failed to retrieve package list: " + _request.Error.message);
						break;
				}
			}
		}
	}
}
