using Db.Builds.Impls;
using Installers.Abstracts;
using UnityEngine;

namespace Installers.Project
{
	[CreateAssetMenu(menuName = "Installers/Project/" + nameof(ProjectDatabaseInstaller), fileName = nameof(ProjectDatabaseInstaller), order = 2)]
	public class ProjectDatabaseInstaller : ADatabaseInstaller
	{
		[SerializeField] private BuildSetting buildSetting;

		public override void InstallBindings()
		{
			BindDatabase(buildSetting);
		}
	}
}