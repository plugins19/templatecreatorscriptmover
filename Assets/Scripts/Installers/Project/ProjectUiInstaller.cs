using Core.Camera.Impls;
using Installers.Abstracts;
using SimpleUi;
using Ui.Project.Loading;
using UnityEngine;

namespace Installers.Project
{
    [CreateAssetMenu(menuName = "Installers/Project/" + nameof(ProjectUiInstaller), fileName = nameof(ProjectUiInstaller), order = 0)]
    public class ProjectUiInstaller : AUiInstaller<ProjectUiCamera>
    {
        [SerializeField] private LoadingView loadingView;

        protected override void BindViewControllers(Transform canvasTransform)
        {
            Container.BindUiView<LoadingController, LoadingView>(loadingView, canvasTransform);
        }
    }
}