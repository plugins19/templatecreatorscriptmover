using Core.Services.Scenes.Impls;
using Core.Utils.CrashChecker.Impls;
using Core.Utils.PlayerPrefs.Impl;
using Core.Utils.Providers.CameraScreenshots.Impls;
using Core.Utils.Providers.Randoms.Impls;
using Core.Utils.Providers.Times.Impls;
using Core.Utils.Screens.Impls;
using Ui.Project.Loading;
using UnityEngine;
using Zenject;

namespace Installers.Project
{
	[CreateAssetMenu(
		menuName = "Installers/Project/" + nameof(ProjectInstaller), fileName = nameof(ProjectInstaller), order = 1
	)]
	public class ProjectInstaller : ScriptableObjectInstaller
	{
		public override void InstallBindings()
		{
			InstallSignalBus();
			BindServices();
			BindWindows();
			BindManagers();
			BindProviders();
			BindUtils();
		}

		private void InstallSignalBus() => SignalBusInstaller.Install(Container);

        private void BindServices()
		{
			Container.BindInterfacesAndSelfTo<SceneService>().AsSingle();
		}

		private void BindWindows()
		{
			Container.BindInterfacesAndSelfTo<LoadingWindow>().AsSingle();
		}

		private void BindManagers()
		{
			Container.BindInterfacesTo<PersistancePlayerPrefsManager>().AsSingle();
		}

		private void BindProviders()
		{
			Container.BindInterfacesTo<UnityTimeProvider>().AsSingle();
			Container.BindInterfacesTo<UnityRandomProvider>().AsSingle();
			Container.BindInterfacesTo<CameraScreenshotProvider>().AsSingle();
		}

		private void BindUtils()
		{
			Container.BindInterfacesTo<CrashChecker>().AsSingle();
			Container.BindInterfacesTo<ScreenUtils>().AsSingle();
		}
	}
}