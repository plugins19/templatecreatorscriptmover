using UnityEngine;
using Zenject;

namespace Installers.Project
{
	[CreateAssetMenu(menuName = "Installers/Project/" + nameof(DaoStorageInstaller), fileName = nameof(DaoStorageInstaller), order = 4)]
	public class DaoStorageInstaller : ScriptableObjectInstaller
	{
		public override void InstallBindings()
		{
            
        }

        private void BindDaoStorage<TStorage, TStorageDao>(string storageName)
		{
			Container.BindInterfacesTo<TStorage>().AsSingle();
            Container.BindInterfacesTo<TStorageDao>().AsSingle().WithArguments(storageName);
        }
	}
}