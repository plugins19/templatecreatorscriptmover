using Core.Camera.Impls;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Installers.Abstracts
{
    public abstract class ASceneUiInstaller : AUiInstaller<SceneUiCamera>
    {
        [SerializeField] private EventSystem eventSystem;
        
        public override void InstallBindings()
        {
            base.InstallBindings();
            Container.InstantiatePrefabForComponent<EventSystem>(eventSystem);
        }
    }
}