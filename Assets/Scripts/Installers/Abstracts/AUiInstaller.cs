using UnityEngine;
using Zenject;

namespace Installers.Abstracts
{
    public abstract class AUiInstaller<TCamera> : ScriptableObjectInstaller
    {
        [SerializeField] private Canvas canvas;

        public override void InstallBindings()
        {
            var canvasInstance = Container.InstantiatePrefabForComponent<Canvas>(canvas);
            var canvasTransform = canvasInstance.transform;
            BindViewControllers(canvasTransform);

            var uiCamera = canvasInstance.transform.GetComponentInChildren<TCamera>();
            Container.BindInterfacesTo<TCamera>().FromInstance(uiCamera).AsSingle();
        }

        protected abstract void BindViewControllers(Transform canvasTransform);
    }
}