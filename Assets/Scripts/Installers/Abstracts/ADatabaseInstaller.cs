using Zenject;

namespace Installers.Abstracts
{
	public abstract class ADatabaseInstaller : ScriptableObjectInstaller
	{
		protected void BindDatabase<TDatabase>(TDatabase database)
		{
			Container.BindInterfacesTo<TDatabase>().FromInstance(database).AsSingle();

			var type = typeof(TDatabase);
			
			if(type.GetInterface(nameof(IInitializable)) == null)
				return;

			Container.BindInitializableExecutionOrder(type, -5000);
		}
	}
}