using Splash.Managers;
using Ui.Splash;
using UnityEngine;
using Zenject;

namespace Installers.Splash
{
    [CreateAssetMenu(menuName = "Installers/Splash/" + nameof(SplashInstaller), fileName = nameof(SplashInstaller), order = 1)]
    public class SplashInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            BindWindows();
            BindManagers();
        }

        private void BindWindows()
        {
            Container.BindInterfacesAndSelfTo<SplashWindow>().AsSingle();
        }

        private void BindManagers()
        {
            Container.BindInterfacesTo<SplashHudOpenerManager>().AsSingle();
        }
    }
}