using Installers.Abstracts;
using SimpleUi;
using Ui.Splash;
using UnityEngine;

namespace Installers.Splash
{
    [CreateAssetMenu(menuName = "Installers/Splash/" + nameof(SplashUiInstaller), fileName = nameof(SplashUiInstaller), order = 0)]
    public class SplashUiInstaller : ASceneUiInstaller
    {
        [SerializeField] private SplashView splashView;
        
        protected override void BindViewControllers(Transform canvasTransform)
        {
            Container.BindUiView<SplashController, SplashView>(splashView, canvasTransform);
        }
    }
}