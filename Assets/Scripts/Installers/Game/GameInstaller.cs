using Game.Managers.Impls;
using Ui.Game.Windows;
using Ui.Handlers.Abstract.Closable.Impls;
using UnityEngine;
using Zenject;

namespace Installers.Game
{
	[CreateAssetMenu(menuName = "Installers/Game/" + nameof(GameInstaller), fileName = nameof(GameInstaller), order = 1)]
	public class GameInstaller : ScriptableObjectInstaller
	{
		public override void InstallBindings()
		{
			BindManagers();
			BindWindows();
			BindHandlers();
		}

		private void BindManagers()
		{
			Container.BindInterfacesTo<GameWindowManager>().AsSingle();
		}

		private void BindWindows()
		{
			Container.BindInterfacesAndSelfTo<GameHudWindow>().AsSingle();
		}
		
		private void BindHandlers()
		{
			//Common
			Container.BindInterfacesTo<ClosableHandler>().AsTransient();
		}
	}
}