using Installers.Abstracts;
using SimpleUi;
using Ui.Game.Main;
using UnityEngine;

namespace Installers.Game
{
    [CreateAssetMenu(menuName = "Installers/Game/" + nameof(GameUiInstaller), fileName = nameof(GameUiInstaller), order = 0)]
    public class GameUiInstaller : ASceneUiInstaller
    {
        [SerializeField] private MainGameView mainGameView;
        
        protected override void BindViewControllers(Transform canvasTransform)
        {
            Container.BindUiView<MainGameController,MainGameView>(mainGameView, canvasTransform);
        }
    }
}