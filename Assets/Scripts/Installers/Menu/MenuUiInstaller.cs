using Installers.Abstracts;
using SimpleUi;
using Ui.Menu.Main;
using UnityEngine;

namespace Installers.Menu
{
	[CreateAssetMenu(menuName = "Installers/Menu/" + nameof(MenuUiInstaller), fileName = nameof(MenuUiInstaller),
		order = 0)]
	public class MenuUiInstaller : ASceneUiInstaller
	{
		[SerializeField] private MainMenuView mainMenuView;

		protected override void BindViewControllers(Transform canvasTransform)
		{
			Container.BindUiView<MainMenuController, MainMenuView>(mainMenuView, canvasTransform);
		}
	}
}