using Installers.Debugs;
using Ui.Menu.Debugs;
using UnityEngine;

namespace Installers.Menu
{
	[CreateAssetMenu(
		menuName = "Installers/Menu/" + nameof(DebugMenuUiInstaller), fileName = nameof(DebugMenuUiInstaller),
		order = 2
	)]
	public class DebugMenuUiInstaller : ADebugScriptableObjectInstaller
	{
		[SerializeField] private Canvas debugCanvas;

		[SerializeField] private DebugView debugView;
		
		protected override void InstallBindingsInternal()
		{
			var debugCanvasInstance = Container.InstantiatePrefabForComponent<Canvas>(debugCanvas);
			var debugCanvasTransform = debugCanvasInstance.transform;
			
			var view = Container.InstantiatePrefabForComponent<DebugView>(debugView, debugCanvasTransform);
			Container.Bind<DebugView>().FromInstance(view).AsSingle();
			Container.BindInterfacesAndSelfTo<DebugController>().AsSingle();
        }
	}
}