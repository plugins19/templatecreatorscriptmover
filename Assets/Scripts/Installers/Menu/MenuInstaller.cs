using Menu.Managers;
using Ui.Handlers.Abstract.Closable.Impls;
using Ui.Menu.Windows;
using UnityEngine;
using Zenject;

namespace Installers.Menu
{
    [CreateAssetMenu(menuName = "Installers/Menu/" + nameof(MenuInstaller), fileName = nameof(MenuInstaller), order = 1)]
    public class MenuInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            BindWindows();
            BindManagers();
            BindHandlers();
        }

        private void BindWindows()
        {
            Container.BindInterfacesAndSelfTo<MainMenuWindow>().AsSingle();
        }

        private void BindManagers()
        {
            Container.BindInterfacesTo<MenuHudOpenerManager>().AsSingle();
        }

        private void BindHandlers()
        {
            //Common
            Container.BindInterfacesTo<ClosableHandler>().AsTransient();
        }
    }
}