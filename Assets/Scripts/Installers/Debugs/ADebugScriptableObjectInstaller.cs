using Db.Builds;
using Zenject;

namespace Installers.Debugs
{
	public abstract class ADebugScriptableObjectInstaller : ScriptableObjectInstaller
	{
		[Inject] private IBuildSetting _buildSetting;
		
		public override void InstallBindings()
		{
			if (_buildSetting.BuildType != EBuildType.Debug)
				return;
			
			InstallBindingsInternal();
		}

		protected abstract void InstallBindingsInternal();
	}
}