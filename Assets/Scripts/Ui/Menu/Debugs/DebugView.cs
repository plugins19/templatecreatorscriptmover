using UnityEngine;
using UnityEngine.UI;

namespace Ui.Menu.Debugs
{
	public class DebugView : MonoBehaviour
	{
		[SerializeField] private Button visibleButton;
		
		[SerializeField] private GameObject content;

		public Button VisibleButton => visibleButton;

		public GameObject Content => content;
	}
}