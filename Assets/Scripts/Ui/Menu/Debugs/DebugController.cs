using UniRx;
using Zenject;

namespace Ui.Menu.Debugs
{
	public class DebugController : IInitializable
	{
		private readonly DebugView _debugView;

        private bool _isViewActive;

		public DebugController(
			DebugView debugView
		)
		{
			_debugView = debugView;
		}

		public void Initialize()
		{
			_debugView.Content.SetActive(_isViewActive);

			_debugView.VisibleButton.OnClickAsObservable()
				.Subscribe(OnVisibleClick)
				.AddTo(_debugView);
		}

		private void OnVisibleClick(Unit obj)
		{
			_isViewActive = !_isViewActive;
			_debugView.Content.SetActive(_isViewActive);
		}
	}
}