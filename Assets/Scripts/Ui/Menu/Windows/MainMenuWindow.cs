using SimpleUi;
using Ui.Menu.Main;

namespace Ui.Menu.Windows
{
    public class MainMenuWindow : WindowBase
    {
        public override string Name => nameof(MainMenuWindow);

        protected override void AddControllers()
        {
            AddController<MainMenuController>();
        }
    }
}