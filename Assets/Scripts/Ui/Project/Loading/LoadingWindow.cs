using SimpleUi;
using SimpleUi.Interfaces;

namespace Ui.Project.Loading
{
    public class LoadingWindow : WindowBase, IPopUp
    {
        public override string Name => nameof(LoadingWindow);
        
        protected override void AddControllers()
        {
            AddController<LoadingController>();
        }
    }
}