using SimpleUi;

namespace Ui.Splash
{
    public class SplashWindow : WindowBase
    {
        public override string Name => nameof(SplashWindow);
        
        protected override void AddControllers()
        {
            AddController<SplashController>();
        }
    }
}