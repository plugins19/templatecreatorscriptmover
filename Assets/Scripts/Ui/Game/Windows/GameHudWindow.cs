using SimpleUi;
using Ui.Game.Main;

namespace Ui.Game.Windows
{
	public class GameHudWindow : WindowBase
	{
		public override string Name => nameof(GameHudWindow);
		
		protected override void AddControllers()
		{
			AddController<MainGameController>();
		}
	}
}