using System;
using Core.Handlers.Main.Impls;
using SimpleUi.Signals;
using UniRx;
using UniRx.Triggers;
using Zenject;

namespace Ui.Handlers.Abstract.Closable.Impls
{
	public class ClosableHandler : APanelHandler<IClosableView>, IClosableHandler
	{
		private readonly SignalBus _signalBus;

		private IDisposable _closeClickDisposable = Disposable.Empty;

		public ClosableHandler(
			SignalBus signalBus
		)
		{
			_signalBus = signalBus;
		}

		public override void Initialize(IClosableView view, EWindowLayer windowLayer = EWindowLayer.Local)
		{
			base.Initialize(view, windowLayer);

			view.CloseButton.OnEnableAsObservable()
				.Subscribe(OnEnable)
				.AddTo(view.CloseButton);

			view.CloseButton.OnDisableAsObservable()
				.Subscribe(OnDisable)
				.AddTo(view.CloseButton);
		}

		private void OnEnable(Unit obj) => _closeClickDisposable = View.CloseButton.OnClickAsObservable()
			.Subscribe(OnCloseClick);

		private void OnDisable(Unit obj) => _closeClickDisposable.Dispose();

		private void OnCloseClick(Unit obj) => _signalBus.BackWindow(WindowLayer);
	}
}