using Core.Handlers.Main;

namespace Ui.Handlers.Abstract.Closable
{
	public interface IClosableHandler : IPanelHandler<IClosableView>
	{
		
	}
}