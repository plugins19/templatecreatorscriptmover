using UnityEngine.UI;

namespace Ui.Handlers.Abstract.Closable
{
	public interface IClosableView
	{
		Button CloseButton { get; }
	}
}