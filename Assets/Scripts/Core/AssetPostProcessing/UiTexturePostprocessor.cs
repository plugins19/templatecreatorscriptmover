#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Core.AssetPostProcessing
{
    public class UiTexturePostprocessor : AssetPostprocessor
    {
        private void OnPreprocessTexture()
        {
            if (!assetPath.EndsWith("UI.png"))
                return;

            var textureImporter = (TextureImporter)assetImporter;
            var textureImporterSettings = new TextureImporterSettings
            {
                textureType = TextureImporterType.Sprite,
                textureShape = TextureImporterShape.Texture2D,
                spriteMode = (int)SpriteImportMode.Single,
                spritePixelsPerUnit = 100,
                spriteMeshType = SpriteMeshType.Tight,
                spriteExtrude = 1,
                spriteGenerateFallbackPhysicsShape = false,
                sRGBTexture = true,
                alphaSource = TextureImporterAlphaSource.FromInput,
                alphaIsTransparency = true,
                ignorePngGamma = false,
                mipmapEnabled = false,
                wrapMode = TextureWrapMode.Clamp,
                filterMode = FilterMode.Bilinear,
                aniso = 1
            };

            TryCutIfCircle(textureImporterSettings);
            TryCutIfCrookedCircle(textureImporterSettings);
            TryCutIfSemiCircle(textureImporterSettings);

            textureImporter.SetTextureSettings(textureImporterSettings);

            ConfigureAndroidTextureSettings(textureImporter);
            ConfigureIOSTextureSettings(textureImporter);
        }

        private void TryCutIfCircle(TextureImporterSettings textureImporterSettings)
        {
            if (!assetPath.Contains("FullCircle"))
                return;

            var startSizeIndex = assetPath.IndexOf('D') + 1;
            var stringSize = string.Empty;

            while (assetPath[startSizeIndex] != '_')
            {
                stringSize += assetPath[startSizeIndex];
                startSizeIndex++;
            }

            var size = int.Parse(stringSize);
            var circleRadius = size / 2;
            textureImporterSettings.spriteBorder = new Vector4(circleRadius, circleRadius, circleRadius, circleRadius);
        }

        private void TryCutIfSemiCircle(TextureImporterSettings textureImporterSettings)
        {
            if (!assetPath.Contains("SemiCircle"))
                return;

            var startSizeIndex = assetPath.IndexOf('D') + 1;
            var stringSize = string.Empty;

            while (assetPath[startSizeIndex] != '_')
            {
                stringSize += assetPath[startSizeIndex];
                startSizeIndex++;
            }

            var size = int.Parse(stringSize);
            var circleRadius = size / 2;
            textureImporterSettings.spriteBorder = new Vector4(circleRadius, circleRadius + 1, circleRadius, 1);
        }

        private void TryCutIfCrookedCircle(TextureImporterSettings textureImporterSettings)
        {
            if (!assetPath.Contains("CrookedCircle"))
                return;

            var startSizeIndex = assetPath.IndexOf('_') + 1;
            var stringSize = string.Empty;

            while (assetPath[startSizeIndex] != '_')
            {
                stringSize += assetPath[startSizeIndex];
                startSizeIndex++;
            }

            var size = int.Parse(stringSize);
            var widthSliceSize = size / 2;
            textureImporterSettings.spriteBorder = new Vector4(widthSliceSize, 0, widthSliceSize, 0);
        }

        private void ConfigureAndroidTextureSettings(TextureImporter textureImporter)
        {
            var androidTextureSettings = textureImporter.GetPlatformTextureSettings("Android");
            androidTextureSettings.overridden = true;
            androidTextureSettings.format = TextureImporterFormat.DXT5Crunched;
            androidTextureSettings.compressionQuality = 100;
            androidTextureSettings.resizeAlgorithm = TextureResizeAlgorithm.Mitchell;
            androidTextureSettings.maxTextureSize = 2048;
            textureImporter.SetPlatformTextureSettings(androidTextureSettings);
        }

        private void ConfigureIOSTextureSettings(TextureImporter textureImporter)
        {
            var iosTextureSettings = textureImporter.GetPlatformTextureSettings("IOS");
            iosTextureSettings.overridden = true;
            iosTextureSettings.format = TextureImporterFormat.ASTC_6x6;
            iosTextureSettings.compressionQuality = 100;
            iosTextureSettings.resizeAlgorithm = TextureResizeAlgorithm.Mitchell;
            iosTextureSettings.maxTextureSize = 2048;
            textureImporter.SetPlatformTextureSettings(iosTextureSettings);
        }
    }
}
#endif