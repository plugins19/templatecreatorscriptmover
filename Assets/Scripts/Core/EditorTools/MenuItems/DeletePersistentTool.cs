#if UNITY_EDITOR

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Core.EditorTools.MenuItems
{
    public class DeletePersistentTool
    {
        [MenuItem("Tools/Clear data/Clear persistent data")]
        private static void DeleteAllPrefs()
        {
            var filePaths = Directory.GetFiles(Application.persistentDataPath);

            foreach (var path in filePaths)
                File.Delete(path);
            
            Debug.Log("Configs clear");
        }
    }
}

#endif