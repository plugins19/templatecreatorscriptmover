#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Core.EditorTools.MenuItems
{
	public class DeleteAllPrefsTool
	{
		[MenuItem("Tools/Clear data/Clear prefs")]
		private static void DeleteAllPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Player prefs clear");
        }
    }
}

#endif

