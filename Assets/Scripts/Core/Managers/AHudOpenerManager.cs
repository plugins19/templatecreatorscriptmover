using SimpleUi;
using SimpleUi.Signals;
using Zenject;

namespace Core.Managers
{
    public class AHudOpenerManager<TWindow> : IInitializable
        where TWindow : WindowBase
    {
        protected readonly SignalBus SignalBus;

        protected AHudOpenerManager(
            SignalBus signalBus
        )
        {
            SignalBus = signalBus;
        }

        public void Initialize() => SignalBus.OpenWindow<TWindow>();
    }
}