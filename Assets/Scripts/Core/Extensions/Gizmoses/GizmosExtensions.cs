using UnityEngine;

namespace Core.Extensions.Gizmoses
{
	public static class GizmosExtensions
	{
		public static void DrawArrow(
			Vector3 pos,
			Vector3 direction,
			float thickness = 1f,
			float arrowHeadLength = 0.2f,
			float arrowHeadAngle = 20.0f
		)
		{
			DrawBoldLine(pos, direction, thickness);

			if (direction == Vector3.zero)
				return;

			var right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0)
			                                               * new Vector3(0, 0, 1);
			var left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0)
			                                              * new Vector3(0, 0, 1);
			DrawBoldLine(pos + direction, right * arrowHeadLength, thickness);
			DrawBoldLine(pos + direction, left * arrowHeadLength, thickness);
		}

		public static void DrawBoldLineBetweenPoints(Vector3 start, Vector3 end, float thickness)
		{
			var direction = end - start;
			DrawBoldLine(start, direction, thickness);
		}

		public static void DrawBoldLine(Vector3 start, Vector3 direction, float thickness)
		{
			var directlyRight = Vector3.zero;
			var directlyLeft = Vector3.zero;

			if (direction != Vector3.zero)
			{
				directlyRight = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + 90, 0)
				                                                   * new Vector3(0, 0, 1);
				directlyLeft = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - 90, 0)
				                                                  * new Vector3(0, 0, 1);
			}

			for (var i = 0; i < thickness; i++)
			{
				var displacement = Mathf.Lerp(-0.05f, 0.05f, i / thickness);
				Gizmos.DrawRay(start + directlyRight * displacement, direction);
				Gizmos.DrawRay(start + directlyLeft * displacement, direction);
			}
		}

		public static void DrawBoxColliderAsWireCube(BoxCollider boxCollider, Color color)
		{
			Gizmos.color = color;
			Gizmos.matrix = boxCollider.transform.localToWorldMatrix;
			Gizmos.DrawWireCube(Vector3.zero, boxCollider.size);
		}
	}
}