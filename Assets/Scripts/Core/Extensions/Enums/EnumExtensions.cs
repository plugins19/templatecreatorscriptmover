using System;

namespace Core.Extensions.Enums
{
	public static class EnumExtensions
	{
		public static int GetLength<TEnum>()
			where TEnum : Enum => Enum.GetValues(typeof(TEnum)).Length;

		public static TEnum GetNextValue<TEnum>(this TEnum current)
			where TEnum : Enum
		{
			var length = GetLength<TEnum>();
			var index = GetIndex(current);
			var nextIndex = (index + 1 + length) % length;
			var enumValues = GetValues<TEnum>();
			return enumValues[nextIndex];
		}

		public static TEnum GetPreviousValue<TEnum>(this TEnum current)
			where TEnum : Enum
		{
			var length = GetLength<TEnum>();
			var index = GetIndex(current);
			var nextIndex = (index - 1 + length) % length;
			var enumValues = GetValues<TEnum>();
			return enumValues[nextIndex];
		}

		public static TEnum[] GetValues<TEnum>()
			where TEnum : Enum => (TEnum[])Enum.GetValues(typeof(TEnum));

		public static int GetIndex<TEnum>(TEnum value)
			where TEnum : Enum
		{
			var enumValues = GetValues<TEnum>();
			var currentIndex = Array.IndexOf(enumValues, value);
			return currentIndex;
		}
	}
}