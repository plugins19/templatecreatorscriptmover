using UnityEngine;

namespace Core.Extensions.Transformes
{
	public static class TransformExtensions
	{
		public static void DestroyAllChildrenImmediately(this Transform parent)
		{
			for (var i = 0; i < parent.childCount; i++)
			{
				var child = parent.GetChild(i);
				Object.DestroyImmediate(child.gameObject);
			}
		}
	}
}