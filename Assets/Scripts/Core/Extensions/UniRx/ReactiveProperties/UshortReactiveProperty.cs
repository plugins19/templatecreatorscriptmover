using UniRx;

namespace Core.Extensions.UniRx.ReactiveProperties
{
	public class UshortReactiveProperty : ReactiveProperty<ushort>
	{
		public UshortReactiveProperty()
		{
		}

		public UshortReactiveProperty(
			ushort initialValue
		) : base(initialValue)
		{
		}
	}
}