namespace Core.Services.Scenes
{
    public enum ESceneType : byte
    {
        Splash = 0,
        Menu = 1,
        Game = 2,
    }
}