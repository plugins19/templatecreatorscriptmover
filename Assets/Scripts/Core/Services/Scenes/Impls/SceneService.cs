using Core.Utils.SceneLoadingProcessor;
using Core.Utils.SceneLoadingProcessor.Extensions;
using Core.Utils.SceneLoadingProcessor.Impls;
using Core.Utils.SceneLoadingProcessor.Impls.Custom;
using SimpleUi.Signals;
using Ui.Project.Loading;
using UnityEngine.SceneManagement;
using Zenject;

namespace Core.Services.Scenes.Impls
{
	public class SceneService : ISceneService
	{
		private readonly SignalBus _signalBus;

		public ESceneType CurrentScene { get; private set; } = ESceneType.Splash;

		public SceneService(
			SignalBus signalBus
		)
		{
			_signalBus = signalBus;
		}

		public IProgressable LoadMenuFromSplash() => new LoadingProcessor()
			.AddProcess(new LoadingProcess(ESceneType.Menu, LoadSceneMode.Additive))
			.AddProcess(new SetActiveSceneProcess(ESceneType.Menu))
			.AddProcess(new UnloadProcess(ESceneType.Splash))
			.Do(SetCurrentSceneAsMenu)
			.AddProcess(new RunContextProcess("MenuContext"))
			.DoProcess();

		public IProgressable LoadSplashFromMenu() => new LoadingProcessor()
			.AddProcess(new LoadingProcess(ESceneType.Splash, LoadSceneMode.Additive))
			.AddProcess(new SetActiveSceneProcess(ESceneType.Splash))
			.AddProcess(new UnloadProcess(ESceneType.Menu))
			.Do(SetCurrentSceneAsSplash)
			.DoProcess();

		public IProgressable LoadGameFromMenu() => new LoadingProcessor()
			.AddProcess(new OpenWindowProcess<LoadingWindow>(_signalBus, EWindowLayer.Project))
			.AddProcess(new LoadingProcess(ESceneType.Game, LoadSceneMode.Additive))
			.AddProcess(new SetActiveSceneProcess(ESceneType.Game))
			.AddProcess(new UnloadProcess(ESceneType.Menu))
			.Do(SetCurrentSceneAsGame)
			.AddProcess(new RunContextProcess("GameContext"))
			.AddProcess(new WaitSecondProcess(3))
			.AddProcess(new BackWindowProcess(_signalBus, EWindowLayer.Project))
			.DoProcess();

		public IProgressable LoadMenuFromGame() => new LoadingProcessor()
			.AddProcess(new OpenWindowProcess<LoadingWindow>(_signalBus, EWindowLayer.Project))
			.AddProcess(new LoadingProcess(ESceneType.Menu, LoadSceneMode.Additive))
			.AddProcess(new SetActiveSceneProcess(ESceneType.Menu))
			.AddProcess(new UnloadProcess(ESceneType.Game))
			.Do(SetCurrentSceneAsMenu)
			.AddProcess(new RunContextProcess("MenuContext"))
			.AddProcess(new WaitSecondProcess(1))
			.AddProcess(new BackWindowProcess(_signalBus, EWindowLayer.Project))
			.DoProcess();

		public IProgressable LoadSplashFromCurrentScene() => new LoadingProcessor()
			.AddProcess(new LoadingProcess(ESceneType.Splash, LoadSceneMode.Additive))
			.AddProcess(new SetActiveSceneProcess(ESceneType.Splash))
			.AddProcess(new UnloadProcess(CurrentScene))
			.Do(SetCurrentSceneAsSplash)
			.DoProcess();

		private void SetCurrentSceneAsSplash() => CurrentScene = ESceneType.Splash;

		private void SetCurrentSceneAsMenu() => CurrentScene = ESceneType.Menu;

		private void SetCurrentSceneAsGame() => CurrentScene = ESceneType.Game;
	}
}