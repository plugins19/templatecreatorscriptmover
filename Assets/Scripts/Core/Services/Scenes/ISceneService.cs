using Core.Utils.SceneLoadingProcessor;

namespace Core.Services.Scenes
{
    public interface ISceneService
    {
        ESceneType CurrentScene { get; }
        
        IProgressable LoadMenuFromSplash();

        IProgressable LoadSplashFromMenu();

        IProgressable LoadGameFromMenu();

        IProgressable LoadMenuFromGame();

        IProgressable LoadSplashFromCurrentScene();
    }
}