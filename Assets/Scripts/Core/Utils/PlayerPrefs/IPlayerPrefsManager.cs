namespace Core.Utils.PlayerPrefs
{
	public interface IPlayerPrefsManager
	{
		int GetInt(string key);
		int GetInt(string key, int defaultValue);
		void SetInt(string key, int value);
		
		long GetLong(string key);
		long GetLong(string key, long defaultValue);
		void SetLong(string key, long value);

		float GetFloat(string key);
		float GetFloat(string key, float defaultValue);
		void SetFloat(string key, float value);

		string GetString(string key);
		string GetString(string key, string defaultValue);
		void SetString(string key, string value);

		bool HasKey(string key);
		void DeleteKey(string key);
		void DeleteAll();
		void Save();
	}
}