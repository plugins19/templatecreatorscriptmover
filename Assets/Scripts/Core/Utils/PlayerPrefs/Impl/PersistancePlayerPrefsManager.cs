namespace Core.Utils.PlayerPrefs.Impl
{
	public class PersistancePlayerPrefsManager : IPlayerPrefsManager
	{
		public int GetInt(string key) => UnityEngine.PlayerPrefs.GetInt(key);

		public int GetInt(string key, int defaultValue)
			=> UnityEngine.PlayerPrefs.GetInt(key, defaultValue);

		public void SetInt(string key, int value)
			=> UnityEngine.PlayerPrefs.SetInt(key, value);

		public long GetLong(string key)
		{
			var longString = UnityEngine.PlayerPrefs.GetString(key);
			return long.Parse(longString);
		}

		public long GetLong(string key, long defaultValue)
		{
			var longString = UnityEngine.PlayerPrefs.GetString(key, defaultValue.ToString());
			return long.Parse(longString);
		}

		public void SetLong(string key, long value)
			=> UnityEngine.PlayerPrefs.SetString(key, value.ToString());

		public float GetFloat(string key) => UnityEngine.PlayerPrefs.GetFloat(key);

		public float GetFloat(string key, float defaultValue)
			=> UnityEngine.PlayerPrefs.GetFloat(key, defaultValue);

		public void SetFloat(string key, float value)
			=> UnityEngine.PlayerPrefs.SetFloat(key, value);

		public string GetString(string key)
			=> UnityEngine.PlayerPrefs.GetString(key);

		public string GetString(string key, string defaultValue)
			=> UnityEngine.PlayerPrefs.GetString(key, defaultValue);

		public void SetString(string key, string value)
			=> UnityEngine.PlayerPrefs.SetString(key, value);

		public bool HasKey(string key) => UnityEngine.PlayerPrefs.HasKey(key);

		public void DeleteKey(string key) => UnityEngine.PlayerPrefs.DeleteKey(key);

		public void DeleteAll() => UnityEngine.PlayerPrefs.DeleteAll();

		public void Save() => UnityEngine.PlayerPrefs.Save();
	}
}