using System;
using Core.Services.Scenes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public class LoadingProcess : Process, IProgressable
	{
		public float Progress => _operation?.progress ?? 0;

		private readonly string _sceneName;
		private readonly LoadSceneMode _mode;
		private AsyncOperation _operation;
		private Action _complete;

		public LoadingProcess(ESceneType sceneType, LoadSceneMode mode)
		{
			_sceneName = sceneType.ToString();
			_mode = mode;
		}

		public override void Do(Action complete)
		{
			_complete = complete;
			_operation = SceneManager.LoadSceneAsync(_sceneName, _mode);
			_operation.completed += OnCompleted;
		}

		private void OnCompleted(AsyncOperation obj)
		{
			_operation.completed -= OnCompleted;
			_complete();
		}
	}
}