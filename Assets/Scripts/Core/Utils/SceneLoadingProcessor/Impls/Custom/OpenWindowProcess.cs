using System;
using SimpleUi;
using SimpleUi.Signals;
using Zenject;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public class OpenWindowProcess<T> : Process where T : WindowBase
	{
		private readonly SignalBus _signalBus;
		private readonly EWindowLayer _windowLayer;

		public OpenWindowProcess(SignalBus signalBus, EWindowLayer windowLayer)
		{
			_signalBus = signalBus;
			_windowLayer = windowLayer;
		}

		public override void Do(Action complete)
		{
			_signalBus.OpenWindow<T>(_windowLayer);
			complete();
		}
	}
}