using System;
using UniRx;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
    public class WaitSecondProcess : Process
    {
        private readonly int _count;

        public WaitSecondProcess(int count = 1)
        {
            _count = count;
        }

        public override void Do(Action onComplete)
        { 
            Observable.Timer(TimeSpan.FromSeconds(_count))
                .Subscribe(_ => onComplete());
        }
    }
}
