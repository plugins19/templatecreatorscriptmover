﻿using System;
using SimpleUi.Signals;
using Zenject;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public class BackWindowProcess : Process
	{
		private readonly SignalBus _signalBus;
		private readonly EWindowLayer _windowLayer;

		public BackWindowProcess(SignalBus signalBus, EWindowLayer windowLayer)
		{
			_signalBus = signalBus;
			_windowLayer = windowLayer;
		}

		public override void Do(Action onComplete)
		{
			_signalBus.BackWindow(_windowLayer);
			onComplete();
		}
	}
}