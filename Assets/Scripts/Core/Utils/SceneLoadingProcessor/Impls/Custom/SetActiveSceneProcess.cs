﻿using System;
using Core.Services.Scenes;
using UnityEngine.SceneManagement;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public class SetActiveSceneProcess : Process
	{
		private readonly string _sceneName;

		public SetActiveSceneProcess(ESceneType sceneType)
		{
			_sceneName = sceneType.ToString();
		}

		public override void Do(Action onComplete)
		{
			var scene = SceneManager.GetSceneByName(_sceneName);
			SceneManager.SetActiveScene(scene);
			onComplete();
		}
	}
}