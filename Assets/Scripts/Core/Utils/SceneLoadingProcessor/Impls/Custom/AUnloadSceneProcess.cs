using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public abstract class AUnloadSceneProcess : Process, IProgressable
	{
		private AsyncOperation _operation;
		private Action _complete;
		
		protected string SceneName { get; set; }

		public float Progress => _operation?.progress ?? 0f;
		
		public override void Do(Action onComplete)
		{
			_complete = onComplete;
			_operation = SceneManager.UnloadSceneAsync(SceneName);
			_operation.completed += OnCompleted;
		}
		
		private void OnCompleted(AsyncOperation obj)
		{
			_operation.completed -= OnCompleted;
			_complete();
		}
	}
}