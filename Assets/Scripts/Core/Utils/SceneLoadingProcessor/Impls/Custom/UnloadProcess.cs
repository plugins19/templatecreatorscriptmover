using Core.Services.Scenes;

namespace Core.Utils.SceneLoadingProcessor.Impls.Custom
{
	public class UnloadProcess : AUnloadSceneProcess
	{
		public UnloadProcess(ESceneType sceneType)
		{
			SceneName = sceneType.ToString();
		}
	}
}