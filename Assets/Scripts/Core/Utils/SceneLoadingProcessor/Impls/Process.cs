using System;

namespace Core.Utils.SceneLoadingProcessor.Impls
{
    public abstract class Process : IProcess
    {
        public abstract void Do(Action onComplete);
    }
}