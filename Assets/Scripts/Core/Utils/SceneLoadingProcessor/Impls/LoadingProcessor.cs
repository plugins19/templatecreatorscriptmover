using System.Collections.Generic;

namespace Core.Utils.SceneLoadingProcessor.Impls
{
    public class LoadingProcessor : ILoadingProcessor
    {
        private readonly Queue<IProcess> _processes = new();

        private readonly List<IProgressable> _progressables = new();

        public float Progress
        {
            get
            {
                if (_progressables.Count == 0)
                    return 0;
                var progress = 0f;
                foreach (var progressable in _progressables)
                    progress += progressable.Progress;
                return progress / _progressables.Count;
            }
        }

        public IProcessor AddProcess(IProcess process)
        {
            _processes.Enqueue(process);
            if (process is IProgressable progressable)
                _progressables.Add(progressable);
            return this;
        }

        public IProgressable DoProcess()
        {
            var process = _processes.Dequeue();
            process.Do(OnComplete);
            return this;
        }

        private void OnComplete()
        {
            if (_processes.Count == 0)
                return;
            DoProcess();
        }
    }
}