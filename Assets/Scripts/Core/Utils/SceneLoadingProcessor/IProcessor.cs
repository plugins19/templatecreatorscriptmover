namespace Core.Utils.SceneLoadingProcessor
{
    public interface IProcessor
    {
        IProcessor AddProcess(IProcess process);
        IProgressable DoProcess();
    }
}