﻿using System;
using Core.Utils.SceneLoadingProcessor.Impls;
using UniRx;

namespace Core.Utils.SceneLoadingProcessor.Extensions.Processes
{
	public abstract class AScheduleOnProcess : Process
	{
		private readonly EFrameType _frameType;

		private Action _onComplete;
		private IDisposable _disposable;

		protected AScheduleOnProcess(EFrameType frameType)
		{
			_frameType = frameType;
		}

		public override void Do(Action onComplete)
		{
			_onComplete = onComplete;
			OnBegin();
			switch (_frameType)
			{
				case EFrameType.Update:
					_disposable = Observable.EveryUpdate().Subscribe(OnFrame);
					break;
				case EFrameType.LateUpdate:
					_disposable = Observable.EveryLateUpdate().Subscribe(OnFrame);
					break;
				case EFrameType.FixedUpdate:
					_disposable = Observable.EveryFixedUpdate().Subscribe(OnFrame);
					break;
				case EFrameType.EndOfFrame:
					_disposable = Observable.EveryEndOfFrame().Subscribe(OnFrame);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void OnFrame(long obj) => OnFrameInternal(OnComplete);

		private void OnComplete()
		{
			_disposable.Dispose();
			_onComplete();
		}

		protected abstract void OnBegin();

		protected abstract void OnFrameInternal(Action onComplete);
	}

	public enum EFrameType
	{
		Update,
		LateUpdate,
		FixedUpdate,
		EndOfFrame
	}
}