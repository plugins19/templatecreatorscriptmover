using System;
using Core.Utils.SceneLoadingProcessor.Impls;

namespace Core.Utils.SceneLoadingProcessor.Extensions.Processes
{
	public class CallbackProcess : Process
	{
		private readonly Action _callback;

		public CallbackProcess(Action callback)
		{
			_callback = callback;
		}

		public override void Do(Action onComplete)
		{
			_callback();
			onComplete();
		}
	}
}