﻿using System;
using UnityEngine;

namespace Core.Utils.SceneLoadingProcessor.Extensions.Processes
{
	public class WaitConditionProcess : AScheduleOnProcess, IProgressable
	{
		private readonly Func<bool> _condition;
		private readonly float _timeout;

		private float _endTimeS;
		public float Progress { get; private set; }

		public WaitConditionProcess(Func<bool> condition, float timeout = -1, EFrameType frameType = EFrameType.Update)
			: base(frameType)
		{
			_condition = condition;
			_timeout = timeout;
		}

		protected override void OnBegin()
		{
			if (_timeout < 0)
				return;

			_endTimeS = Time.realtimeSinceStartup + _timeout;
		}

		protected override void OnFrameInternal(Action onComplete)
		{
			Progress = _timeout > 0 ? Mathf.Clamp01((_endTimeS - Time.realtimeSinceStartup) / _timeout) : 0;
			if (_condition())
			{
				onComplete();
				return;
			}

			if (_timeout < 0)
				return;

			if (_endTimeS > Time.realtimeSinceStartup)
				return;

			onComplete();
		}
	}
}