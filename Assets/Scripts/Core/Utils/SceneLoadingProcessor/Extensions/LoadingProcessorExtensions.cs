﻿using System;
using Core.Utils.SceneLoadingProcessor.Extensions.Processes;

namespace Core.Utils.SceneLoadingProcessor.Extensions
{
	public static class LoadingProcessorExtensions
	{
		public static IProcessor WaitCondition(
			this IProcessor processor,
			Func<bool> condition,
			float timeout = -1,
			EFrameType frameType = EFrameType.Update
		)
			=> processor.AddProcess(new WaitConditionProcess(condition, timeout, frameType));

		public static IProcessor Do(this IProcessor processor, Action callback)
			=> processor.AddProcess(new CallbackProcess(callback));
	}
}