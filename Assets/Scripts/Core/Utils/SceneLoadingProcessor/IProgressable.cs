namespace Core.Utils.SceneLoadingProcessor
{
    public interface IProgressable
    {
        float Progress { get; }
    }
}