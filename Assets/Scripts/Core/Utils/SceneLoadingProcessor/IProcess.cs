using System;

namespace Core.Utils.SceneLoadingProcessor
{
    public interface IProcess
    {
        void Do(Action complete);
    }
}