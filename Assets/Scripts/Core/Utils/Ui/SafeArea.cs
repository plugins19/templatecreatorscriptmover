﻿using System;
using UnityEngine;

namespace Core.Utils.Ui
{
	public class SafeArea : MonoBehaviour
	{
		[Header("SafeArea")] 
		[SerializeField] private bool xMin = true;
		[SerializeField] private bool xMax = true;
		[SerializeField] private bool yMin = true;
		[SerializeField] private bool yMax = true;

		[Header("Orientation")] 
		[SerializeField] private bool portraitUp = true;
		[SerializeField] private bool portraitDown = true;
		[SerializeField] private bool landscapeRight = true;
		[SerializeField] private bool landscapeLeft = true;

		private RectTransform _panel;
		private Rect _lastSafeArea = new(0, 0, 0, 0);

		private void Awake()
		{
			_panel = GetComponent<RectTransform>();
			Refresh();
		}

		private void Update() => Refresh();

        private void Refresh()
		{
			var safeArea = IsTriggeredOrientation()
				? Screen.safeArea
				: new Rect(0, 0, Screen.width, Screen.height);
			if (safeArea != _lastSafeArea)
				ApplySafeArea(safeArea);
		}

		private bool IsTriggeredOrientation()
        {
            var orientation = Screen.orientation;
            return orientation switch
            {
                ScreenOrientation.AutoRotation => false,
                ScreenOrientation.Portrait => portraitUp,
                ScreenOrientation.PortraitUpsideDown => portraitDown,
                ScreenOrientation.LandscapeLeft => landscapeLeft,
                ScreenOrientation.LandscapeRight => landscapeRight,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

		private void ApplySafeArea(Rect r)
		{
			_lastSafeArea = r;

			// Convert safe area rectangle from absolute pixels to normalised anchor coordinates
			var anchorMin = _panel.anchorMin;
			var anchorMax = _panel.anchorMax;
			var positionMin = r.position;
			var positionMax = r.position + r.size;

			//TODO: Flags
			if (yMin)
				anchorMin.y = positionMin.y / Screen.height;
			if (yMax)
				anchorMax.y = positionMax.y / Screen.height;
			if (xMin)
				anchorMin.x = positionMin.x / Screen.width;
			if (xMax)
				anchorMax.x = positionMax.x / Screen.width;

			_panel.anchorMin = anchorMin;
			_panel.anchorMax = anchorMax;
		}
	}
}