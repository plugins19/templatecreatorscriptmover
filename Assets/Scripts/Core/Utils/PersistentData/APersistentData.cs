using System;
using Core.Utils.CrashChecker;
using Core.Utils.Dao;
using UniRx;

namespace Core.Utils.PersistentData
{
	public abstract class APersistentData<TVo> : IPersistentData
	{
		private readonly IDao<TVo> _dao;
		private readonly ICrashChecker _crashChecker;
		
		private IDisposable _persistWaitDisposable;

		protected virtual bool BlockSaveOnException => false;

		protected APersistentData(
			IDao<TVo> dao,
			ICrashChecker crashChecker
		)
		{
			_dao = dao;
			_crashChecker = crashChecker;
		}

		public void SetDirty()
		{
			if (_persistWaitDisposable != null)
				return;

			_persistWaitDisposable = Observable.NextFrame(FrameCountType.EndOfFrame).Subscribe(OnEndOfFrame);
		}

		private void OnEndOfFrame(Unit obj) => PersistImmediate();

		public void PersistImmediate()
		{
			_persistWaitDisposable?.Dispose();
			_persistWaitDisposable = null;

			if (_crashChecker.HasExceptions && BlockSaveOnException)
				return;

			var vo = ConvertToJo();
			_dao.Save(vo);
		}

		protected abstract TVo ConvertToJo();
	}
}