namespace Core.Utils.PersistentData
{
	public interface IPersistentData
	{
		void SetDirty();

		void PersistImmediate();
	}
}