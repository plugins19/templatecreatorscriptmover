using UnityEngine;

namespace Core.Utils.Providers.Randoms.Impls
{
	public class UnityRandomProvider : IRandomProvider
	{
		public float Value => Random.value;

		public int Range(int min, int max) => Random.Range(min, max);
		
		public int Range(int max) => Random.Range(0, max);
		
		public short Range(short max) => (short) Random.Range(0, max);

		public float Range(float min, float max) => Random.Range(min, max);
		
		public float Range(float max) => Random.Range(0, max);

		public Vector2 GetVector2() => Random.insideUnitCircle;
	}
}