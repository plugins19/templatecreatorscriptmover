using UnityEngine;

namespace Core.Utils.Providers.Randoms
{
	public interface IRandomProvider
	{
		float Value { get; }
		
		int Range(int min, int max);
		
		int Range(int max);
		
		short Range(short max);
		
		float Range(float min, float max);
		
		float Range(float max);
		
		Vector2 GetVector2();
	}
}