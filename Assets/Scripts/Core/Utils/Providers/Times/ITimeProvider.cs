namespace Core.Utils.Providers.Times
{
	public interface ITimeProvider
	{
		float Time { get; }
		
		float UnscaledTime { get; }
		
		float DeltaTime { get; }
		
		float UnscaledDeltaTime { get; }
		
		float FixedDeltaTime { get; }
	}
}