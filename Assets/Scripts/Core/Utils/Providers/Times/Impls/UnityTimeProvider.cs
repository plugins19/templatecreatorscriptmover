namespace Core.Utils.Providers.Times.Impls
{
	public class UnityTimeProvider : ITimeProvider
	{
		public float Time => UnityEngine.Time.time;
		public float UnscaledTime => UnityEngine.Time.unscaledTime;
		public float DeltaTime => UnityEngine.Time.deltaTime;
		public float UnscaledDeltaTime => UnityEngine.Time.unscaledDeltaTime;
		public float FixedDeltaTime => UnityEngine.Time.fixedDeltaTime;
	}
}