using UnityEngine;

namespace Core.Utils.Providers.CameraScreenshots
{
	public interface ICameraScreenshotProvider
	{
		Texture2D CreateScreenshot(UnityEngine.Camera camera);
	}
}