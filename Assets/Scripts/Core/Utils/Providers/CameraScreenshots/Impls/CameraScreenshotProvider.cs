using Core.Utils.Screens;
using UnityEngine;

namespace Core.Utils.Providers.CameraScreenshots.Impls
{
	public class CameraScreenshotProvider : ICameraScreenshotProvider
	{
		private readonly IScreenUtils _screenUtils;

		public CameraScreenshotProvider(
			IScreenUtils screenUtils
		)
		{
			_screenUtils = screenUtils;
		}

		public Texture2D CreateScreenshot(UnityEngine.Camera camera)
		{
			var width = _screenUtils.Width;
			var height = _screenUtils.Height;
			var renderTexture = RenderTexture.GetTemporary(width, height, 24);
			camera.targetTexture = renderTexture;
			var texture2D = new Texture2D(width, height, TextureFormat.RGB24, false);
			camera.Render();
			RenderTexture.active = renderTexture;
			texture2D.ReadPixels(new Rect(0, 0, width, height), 0, 0);
			texture2D.Apply();
			camera.targetTexture = null;
			RenderTexture.active = null;
			RenderTexture.ReleaseTemporary(renderTexture);
			return texture2D;
		}
	}
}