using System.Collections.Generic;

namespace Core.Utils.Providers
{
	public static class IDGenerator
	{
		private static readonly HashSet<int> _reservedIDs = new HashSet<int>();
		private static int _current;

		private static int NextUid
		{
			get
			{
				if (_current == int.MaxValue)
					_current = 1;
				return ++_current;
			}
		}

		public static int Next()
		{
			int id;
			do
			{
				id = NextUid;
			} while (_reservedIDs.Contains(id));

			_reservedIDs.Add(id);
			return id;
		}

		public static void Reserve(int id) => _reservedIDs.Add(id);

		public static void Remove(int id) => _reservedIDs.Remove(id);

		public static void Clear()
		{
			_reservedIDs.Clear();
			_current = 0;
		}
	}
}