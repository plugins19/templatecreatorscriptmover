using System.Collections.Generic;

namespace Core.Utils.ByteConverting
{
	public static class ByteWriterExtensions
  {
    public static void Write(this ByteWriter writer, byte? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, sbyte? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, short? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, ushort? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, int? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, int[] array)
    {
      writer.Write((ushort) array.Length);
      foreach (int num in array)
        writer.Write(num);
    }

    public static void Write(this ByteWriter writer, int?[] array)
    {
      writer.Write((ushort) array.Length);
      foreach (int? nullable in array)
        writer.Write(nullable);
    }

    public static void Write(this ByteWriter writer, uint? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, long? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, ulong? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, float? value)
    {
      if (value.HasValue)
      {
        writer.Write(true);
        writer.Write(value.Value);
      }
      else
        writer.Write(false);
    }

    public static void Write(this ByteWriter writer, float[] array)
    {
      writer.Write((ushort) array.Length);
      for (int index = 0; index < array.Length; ++index)
        writer.Write(array[index]);
    }

    public static void Write(this ByteWriter writer, float?[] array)
    {
      writer.Write((ushort) array.Length);
      foreach (float? nullable in array)
        writer.Write(nullable);
    }

    public static void Write<T>(this ByteWriter writer, IList<T> list) where T : IByteConvertable
    {
      writer.Write((ushort) list.Count);
      foreach (T obj in (IEnumerable<T>) list)
        obj.ToByte(writer);
    }

    public static void Write(this ByteWriter writer, IByteConvertable convertable) => convertable.ToByte(writer);
  }
}