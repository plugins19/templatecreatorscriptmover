using System;
using System.Text;

namespace Core.Utils.ByteConverting
{
	public sealed class ByteWriter
  {
    private readonly ByteBuffer _buffer;
    private UIntFloat _floatConverter;

    public ByteWriter() => this._buffer = new ByteBuffer();

    public ByteWriter(byte[] buffer) => this._buffer = new ByteBuffer(buffer);

    private ByteWriter(ByteBuffer buffer) => this._buffer = buffer;

    public int Position => this._buffer.Position;

    public byte[] ToArray() => this._buffer.CopyArraySegment();

    public ArraySegment<byte> AsArraySegment() => this._buffer.AsArraySegment();

    public void Write(char value) => this._buffer.WriteByte((byte) value);

    public void Write(byte value) => this._buffer.WriteByte(value);

    public void Write(sbyte value) => this._buffer.WriteByte((byte) value);

    public void Write(short value) => this._buffer.WriteByte2((byte) ((uint) value & (uint) byte.MaxValue), (byte) ((int) value >> 8 & (int) byte.MaxValue));

    public void Write(ushort value) => this._buffer.WriteByte2((byte) ((uint) value & (uint) byte.MaxValue), (byte) ((int) value >> 8 & (int) byte.MaxValue));

    public void Write(int value) => this._buffer.WriteByte4((byte) (value & (int) byte.MaxValue), (byte) (value >> 8 & (int) byte.MaxValue), (byte) (value >> 16 & (int) byte.MaxValue), (byte) (value >> 24 & (int) byte.MaxValue));

    public void Write(uint value) => this._buffer.WriteByte4((byte) (value & (uint) byte.MaxValue), (byte) (value >> 8 & (uint) byte.MaxValue), (byte) (value >> 16 & (uint) byte.MaxValue), (byte) (value >> 24 & (uint) byte.MaxValue));

    public void Write(long value) => this._buffer.WriteByte8((byte) ((ulong) value & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 8) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 16) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 24) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 32) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 40) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 48) & (ulong) byte.MaxValue), (byte) ((ulong) (value >> 56) & (ulong) byte.MaxValue));

    public void Write(ulong value) => this._buffer.WriteByte8((byte) (value & (ulong) byte.MaxValue), (byte) (value >> 8 & (ulong) byte.MaxValue), (byte) (value >> 16 & (ulong) byte.MaxValue), (byte) (value >> 24 & (ulong) byte.MaxValue), (byte) (value >> 32 & (ulong) byte.MaxValue), (byte) (value >> 40 & (ulong) byte.MaxValue), (byte) (value >> 48 & (ulong) byte.MaxValue), (byte) (value >> 56 & (ulong) byte.MaxValue));

    public void Write(float value)
    {
      this._floatConverter.floatValue = value;
      this.Write(this._floatConverter.intValue);
    }

    public void Write(double value)
    {
      this._floatConverter.doubleValue = value;
      this.Write(this._floatConverter.longValue);
    }

    public void Write(Decimal value)
    {
      int[] bits = Decimal.GetBits(value);
      this.Write(bits[0]);
      this.Write(bits[1]);
      this.Write(bits[2]);
      this.Write(bits[3]);
    }

    public void Write(bool value) => this._buffer.WriteByte(value ? (byte) 1 : (byte) 0);

    public void PushBytes(byte[] buffer) => this._buffer.WriteBytes(buffer, buffer.Length);

    public void PushBytes(byte[] buffer, int count) => this._buffer.WriteBytes(buffer, count);

    public void PushBytes(byte[] buffer, int offset, int count) => this._buffer.WriteBytes(buffer, offset, count);

    public void Write(byte[] buffer)
    {
      if (buffer == null || buffer.Length == 0)
      {
        this.Write((ushort) 0);
      }
      else
      {
        this.Write((ushort) buffer.Length);
        this._buffer.WriteBytes(buffer, buffer.Length);
      }
    }

    public void Write(string value) => this.Write(Encoding.UTF8.GetBytes(value));

    public void SeekZero() => this._buffer.SeekZero();

    public void Skip(int length) => this._buffer.Skip(length);

    public ByteWriter Copy() => new ByteWriter(this._buffer.Copy());
  }
}