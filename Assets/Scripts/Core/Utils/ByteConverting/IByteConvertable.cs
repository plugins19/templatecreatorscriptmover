namespace Core.Utils.ByteConverting
{
	public interface IByteConvertable
	{
		void ToByte(ByteWriter writer);

		void FromByte(ByteReader reader);
	}
}