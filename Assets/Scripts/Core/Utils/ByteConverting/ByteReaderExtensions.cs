using System.Collections.Generic;

namespace Core.Utils.ByteConverting
{
	public static class ByteReaderExtensions
  {
    public static byte? ReadByteNullable(this ByteReader reader) => !reader.ReadBoolean() ? new byte?() : new byte?(reader.ReadByte());

    public static void SkipByteNullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(1);
    }

    public static sbyte? ReadSByteNullable(this ByteReader reader) => !reader.ReadBoolean() ? new sbyte?() : new sbyte?(reader.ReadSByte());

    public static void SkipSByteNullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(1);
    }

    public static short? ReadInt16Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new short?() : new short?(reader.ReadInt16());

    public static void SkipInt16Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(2);
    }

    public static ushort? ReadUInt16Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new ushort?() : new ushort?(reader.ReadUInt16());

    public static void SkipUInt16Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(2);
    }

    public static uint? ReadUInt32Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new uint?() : new uint?(reader.ReadUInt32());

    public static void SkipUInt32Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(4);
    }

    public static long? ReadInt64Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new long?() : new long?(reader.ReadInt64());

    public static void SkipInt64Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(8);
    }

    public static ulong? ReadUInt64Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new ulong?() : new ulong?(reader.ReadUInt64());

    public static void SkipUInt64Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(8);
    }

    public static float? ReadSingleNullable(this ByteReader reader) => !reader.ReadBoolean() ? new float?() : new float?(FloatConversion.ToSingle(reader.ReadUInt32()));

    public static void SkipSingleNullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(4);
    }

    public static int? ReadInt32Nullable(this ByteReader reader) => !reader.ReadBoolean() ? new int?() : new int?(reader.ReadInt32());

    public static void SkipInt32Nullable(this ByteReader reader)
    {
      if (!reader.ReadBoolean())
        return;
      reader.Skip(4);
    }

    public static int[] ReadInt32Array(this ByteReader reader)
    {
      ushort length = reader.ReadUInt16();
      int[] numArray = new int[(int) length];
      for (int index = 0; index < (int) length; ++index)
        numArray[index] = reader.ReadInt32();
      return numArray;
    }

    public static void SkipInt32Array(this ByteReader reader)
    {
      ushort num = reader.ReadUInt16();
      reader.Skip((int) num * 4);
    }

    public static int?[] ReadInt32NullableArray(this ByteReader reader)
    {
      ushort length = reader.ReadUInt16();
      int?[] nullableArray = new int?[(int) length];
      for (int index = 0; index < (int) length; ++index)
        nullableArray[index] = reader.ReadInt32Nullable();
      return nullableArray;
    }

    public static void SkipInt32NullableArray(this ByteReader reader)
    {
      ushort num = reader.ReadUInt16();
      for (int index = 0; index < (int) num; ++index)
        reader.SkipInt32Nullable();
    }

    public static float[] ReadSingleArray(this ByteReader reader)
    {
      ushort length = reader.ReadUInt16();
      float[] numArray = new float[(int) length];
      for (int index = 0; index < (int) length; ++index)
        numArray[index] = reader.ReadSingle();
      return numArray;
    }

    public static void SkipSingleArray(this ByteReader reader)
    {
      ushort num = reader.ReadUInt16();
      reader.Skip((int) num * 4);
    }

    public static float?[] ReadSingleNullableArray(this ByteReader reader)
    {
      ushort length = reader.ReadUInt16();
      float?[] nullableArray = new float?[(int) length];
      for (int index = 0; index < (int) length; ++index)
        nullableArray[index] = reader.ReadSingleNullable();
      return nullableArray;
    }

    public static void SkipSingleNullableArray(this ByteReader reader)
    {
      ushort num = reader.ReadUInt16();
      for (int index = 0; index < (int) num; ++index)
        reader.SkipSingleNullable();
    }

    public static List<T> ReadList<T>(this ByteReader reader) where T : IByteConvertable, new()
    {
      ushort capacity = reader.ReadUInt16();
      List<T> objList = new List<T>((int) capacity);
      for (int index = 0; index < (int) capacity; ++index)
      {
        T obj = new T();
        obj.FromByte(reader);
        objList.Add(obj);
      }
      return objList;
    }

    public static T[] ReadArray<T>(this ByteReader reader) where T : IByteConvertable, new()
    {
      ushort length = reader.ReadUInt16();
      T[] objArray = new T[(int) length];
      for (int index = 0; index < (int) length; ++index)
      {
        T obj = new T();
        obj.FromByte(reader);
        objArray[index] = obj;
      }
      return objArray;
    }

    public static T Read<T>(this ByteReader reader) where T : IByteConvertable, new()
    {
      T obj = new T();
      obj.FromByte(reader);
      return obj;
    }
  }
}