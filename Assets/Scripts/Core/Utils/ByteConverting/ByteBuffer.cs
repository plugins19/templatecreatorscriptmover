using System;

namespace Core.Utils.ByteConverting
{
	public sealed class ByteBuffer : ICloneable
  {
    private const int BUFFER_SIZE_LIMIT = 65535;
    private const int INITIAL_SIZE = 64;
    private byte[] _buffer;
    private int _position;

    public ByteBuffer()
      : this(new byte[64])
    {
    }

    public ByteBuffer(byte[] buffer, int position = 0)
    {
      this._buffer = buffer;
      this._position = position;
    }

    public int Position => this._position;

    public int Length => this._buffer.Length;

    public byte ReadByte()
    {
      if (this._position >= this._buffer.Length)
        throw new IndexOutOfRangeException("NetworkReader:ReadByte out of range:" + this.ToString());
      return this._buffer[this._position++];
    }

    public void ReadBytes(byte[] buffer, int count)
    {
      if (this._position + count > this._buffer.Length)
        throw new IndexOutOfRangeException("NetworkReader:ReadBytes out of range: (" + count.ToString() + ") " + this.ToString());
      Buffer.BlockCopy((Array) this._buffer, this._position, (Array) buffer, 0, count);
      this._position += count;
    }

    public byte[] CopyArraySegment()
    {
      byte[] dst = new byte[this._position];
      Buffer.BlockCopy((Array) this._buffer, 0, (Array) dst, 0, this._position);
      return dst;
    }

    public ArraySegment<byte> AsArraySegment() => new ArraySegment<byte>(this.CopyArraySegment(), 0, this._position);

    public void WriteByte(byte value)
    {
      this.WriteCheckForSpace(1);
      this._buffer[this._position] = value;
      ++this._position;
    }

    public void WriteByte2(byte value0, byte value1)
    {
      this.WriteCheckForSpace(2);
      this._buffer[this._position] = value0;
      this._buffer[this._position + 1] = value1;
      this._position += 2;
    }

    public void WriteByte4(byte value0, byte value1, byte value2, byte value3)
    {
      this.WriteCheckForSpace(4);
      this._buffer[this._position] = value0;
      this._buffer[this._position + 1] = value1;
      this._buffer[this._position + 2] = value2;
      this._buffer[this._position + 3] = value3;
      this._position += 4;
    }

    public void WriteByte8(
      byte value0,
      byte value1,
      byte value2,
      byte value3,
      byte value4,
      byte value5,
      byte value6,
      byte value7)
    {
      this.WriteCheckForSpace(8);
      this._buffer[this._position] = value0;
      this._buffer[this._position + 1] = value1;
      this._buffer[this._position + 2] = value2;
      this._buffer[this._position + 3] = value3;
      this._buffer[this._position + 4] = value4;
      this._buffer[this._position + 5] = value5;
      this._buffer[this._position + 6] = value6;
      this._buffer[this._position + 7] = value7;
      this._position += 8;
    }

    public void WriteBytes(byte[] buffer, int count) => this.WriteBytes(buffer, 0, count);

    public void WriteBytes(byte[] buffer, int offset, int count)
    {
      this.WriteCheckForSpace(count);
      Buffer.BlockCopy((Array) buffer, offset, (Array) this._buffer, this._position, count);
      this._position += count;
    }

    private void WriteCheckForSpace(int count)
    {
      int num = this._position + count;
      if (num < this._buffer.Length)
        return;
      do
      {
        int newSize = this._buffer.Length << 1;
        if (newSize > (int) ushort.MaxValue)
          throw new Exception(string.Format("[{0}] Data in buffer should be less or equal size {1}", (object) nameof (ByteBuffer), (object) (int) ushort.MaxValue));
        Array.Resize<byte>(ref this._buffer, newSize);
      }
      while (num > this._buffer.Length);
    }

    public void SeekZero() => this._position = 0;

    public void Skip(int length) => this._position += length;

    public void Replace(byte[] buffer)
    {
      this._buffer = buffer;
      this._position = 0;
    }

    public override string ToString() => string.Format("NetBuf sz:{0} pos:{1}", (object) this._buffer.Length, (object) this._position);

    object ICloneable.Clone() => (object) this.Clone();

    public ByteBuffer Clone() => new ByteBuffer(this._buffer, this._position);

    public ByteBuffer Copy()
    {
      byte[] buffer = new byte[this._buffer.Length];
      this._buffer.CopyTo((Array) buffer, 0);
      return new ByteBuffer(buffer, this._position);
    }
  }
}