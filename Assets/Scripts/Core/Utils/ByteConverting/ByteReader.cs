using System;
using System.Text;

namespace Core.Utils.ByteConverting
{
	public sealed class ByteReader : ICloneable
  {
    private readonly ByteBuffer _buffer;

    public ByteReader() => this._buffer = new ByteBuffer();

    public ByteReader(byte[] buffer) => this._buffer = new ByteBuffer(buffer);

    private ByteReader(ByteBuffer buffer) => this._buffer = buffer;

    public int Position => this._buffer.Position;

    public int Length => this._buffer.Length;

    public void SeekZero() => this._buffer.SeekZero();

    public void Skip(int length) => this._buffer.Skip(length);

    public void Replace(byte[] buffer) => this._buffer.Replace(buffer);

    public byte ReadByte() => this._buffer.ReadByte();

    public void SkipByte() => this._buffer.Skip(1);

    public sbyte ReadSByte() => (sbyte) this._buffer.ReadByte();

    public void SkipSByte() => this._buffer.Skip(1);

    public short ReadInt16() => (short) (ushort) ((uint) (ushort) (0U | (uint) this._buffer.ReadByte()) | (uint) (ushort) ((uint) this._buffer.ReadByte() << 8));

    public void SkipInt16() => this._buffer.Skip(2);

    public ushort ReadUInt16() => (ushort) ((uint) (ushort) (0U | (uint) this._buffer.ReadByte()) | (uint) (ushort) ((uint) this._buffer.ReadByte() << 8));

    public void SkipUInt16() => this._buffer.Skip(2);

    public int ReadInt32() => 0 | (int) this._buffer.ReadByte() | (int) this._buffer.ReadByte() << 8 | (int) this._buffer.ReadByte() << 16 | (int) this._buffer.ReadByte() << 24;

    public void SkipInt32() => this._buffer.Skip(4);

    public uint ReadUInt32() => (uint) (0 | (int) this._buffer.ReadByte() | (int) this._buffer.ReadByte() << 8 | (int) this._buffer.ReadByte() << 16 | (int) this._buffer.ReadByte() << 24);

    public void SkipUInt32() => this._buffer.Skip(4);

    public long ReadInt64() => 0L | (long) this._buffer.ReadByte() | (long) this._buffer.ReadByte() << 8 | (long) this._buffer.ReadByte() << 16 | (long) this._buffer.ReadByte() << 24 | (long) this._buffer.ReadByte() << 32 | (long) this._buffer.ReadByte() << 40 | (long) this._buffer.ReadByte() << 48 | (long) this._buffer.ReadByte() << 56;

    public void SkipInt64() => this._buffer.Skip(8);

    public ulong ReadUInt64() => (ulong) (0L | (long) this._buffer.ReadByte() | (long) this._buffer.ReadByte() << 8 | (long) this._buffer.ReadByte() << 16 | (long) this._buffer.ReadByte() << 24 | (long) this._buffer.ReadByte() << 32 | (long) this._buffer.ReadByte() << 40 | (long) this._buffer.ReadByte() << 48 | (long) this._buffer.ReadByte() << 56);

    public void SkipUInt64() => this._buffer.Skip(8);

    public Decimal ReadDecimal() => new Decimal(new int[4]
    {
      this.ReadInt32(),
      this.ReadInt32(),
      this.ReadInt32(),
      this.ReadInt32()
    });

    public void SkipDecimal() => this._buffer.Skip(16);

    public float ReadSingle() => FloatConversion.ToSingle(this.ReadUInt32());

    public void SkipSingle() => this._buffer.Skip(4);

    public double ReadDouble() => FloatConversion.ToDouble(this.ReadUInt64());

    public void SkipDouble() => this._buffer.Skip(8);

    public char ReadChar() => (char) this._buffer.ReadByte();

    public void SkipChar() => this._buffer.Skip(1);

    public bool ReadBoolean() => this._buffer.ReadByte() == (byte) 1;

    public void SkipBoolean() => this._buffer.Skip(1);

    public byte[] ReadBytes(int count)
    {
      byte[] buffer = count >= 0 ? new byte[count] : throw new IndexOutOfRangeException("NetworkReader ReadBytes " + count.ToString());
      this._buffer.ReadBytes(buffer, count);
      return buffer;
    }

    public byte[] ReadBytesAndSize()
    {
      ushort count = this.ReadUInt16();
      return count != (ushort) 0 ? this.ReadBytes((int) count) : Array.Empty<byte>();
    }

    public void SkipBytesAndSize() => this._buffer.Skip((int) this.ReadUInt16());

    public string ReadString() => Encoding.UTF8.GetString(this.ReadBytesAndSize());

    public void SkipString() => this.SkipBytesAndSize();

    public override string ToString() => this._buffer.ToString();

    object ICloneable.Clone() => (object) this.Clone();

    public ByteReader Clone() => new ByteReader(this._buffer.Clone());
  }
}