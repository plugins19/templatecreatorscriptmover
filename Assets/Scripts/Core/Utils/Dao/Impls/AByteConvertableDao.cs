﻿using Core.Utils.ByteConverting;

namespace Core.Utils.Dao.Impls
{
	public abstract class AByteConvertableDao<T> : IDao<T>
		where T : IByteConvertable, new()
	{
		private readonly BytesStorageDao _byteDao;

		protected AByteConvertableDao(string fileName)
		{
			_byteDao = new BytesStorageDao(fileName);
		}

		public bool Exists() => _byteDao.Exists();

		public void Save(T obj)
		{
			var writer = new ByteWriter();
			obj.ToByte(writer);
			var bytes = writer.ToArray();
			_byteDao.Save(bytes);
		}

		public T Load(T defaultValue = default)
		{
			if (!Exists())
				return defaultValue;

			var bytes = _byteDao.Load();
			var obj = new T();
			var reader = new ByteReader(bytes);
			obj.FromByte(reader);
			return obj;
		}

		public void Remove() => _byteDao.Remove();
	}
}