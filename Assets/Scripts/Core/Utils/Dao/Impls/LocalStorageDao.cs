using System;
using System.IO;
using UnityEngine;

namespace Core.Utils.Dao.Impls
{
	public class LocalStorageDao<T> : IDao<T>
	{
		private readonly string _filename;

		public LocalStorageDao(string filename)
		{
			_filename = filename;
		}

		public bool Exists() => File.Exists(GetPath());

		public void Save(T obj)
		{
			var json = JsonUtility.ToJson(obj);
			var path = GetPath();
			FileUtils.CreateDirectoryAndWriteAllText(path, json);
		}

		public T Load(T defaultValue = default)
		{
			if (!Exists())
				return defaultValue;

			var path = GetPath();
			var json = File.ReadAllText(path);
			try
			{
				return JsonUtility.FromJson<T>(json);
			}
			catch (Exception e)
			{
				Debug.LogError($"[{GetType().Name}] {e.Message} {e.StackTrace}");
				Remove();
				return defaultValue;
			}
		}

		public void Remove()
		{
			var path = GetPath();
			FileUtils.DeleteFile(path);
		}

		private string GetPath() => Path.Combine(Application.persistentDataPath, _filename);
	}
}