using System.IO;
using UnityEngine;

namespace Core.Utils.Dao.Impls
{
	public class BytesStorageDao : IBytesStorageDao
	{
		private readonly string _path;

		public BytesStorageDao(string filename)
		{
			_path = Path.Combine(Application.persistentDataPath, filename);
		}

		public bool Exists() => File.Exists(_path);

		public void Save(byte[] obj) => FileUtils.CreateDirectoryAndWriteAllBytes(_path, obj);

		public byte[] Load(byte[] defaultValue = default) => !Exists() ? defaultValue : File.ReadAllBytes(_path);

		public void Remove() => FileUtils.DeleteFile(_path);
	}
}