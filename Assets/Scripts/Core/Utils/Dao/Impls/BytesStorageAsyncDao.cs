﻿using System;
using System.IO;
using UniRx;
using UnityEngine;

namespace Core.Utils.Dao.Impls
{
	/// <summary>
	///     Working incorrect, do not use this!!!
	/// </summary>
	public class BytesStorageAsyncDao : IAsyncDao<byte[]>
	{
		private const string BACKUP_PREFIX = ".bak";
		private const string TEMP_PREFIX = ".tmp";

		private readonly string _path;
		private readonly string _pathBackup;
		private readonly string _pathTemp;

		public BytesStorageAsyncDao(string filename)
		{
			_path = Path.Combine(Application.persistentDataPath, filename);
			_pathBackup = Path.Combine(Application.persistentDataPath, filename + BACKUP_PREFIX);
			_pathTemp = Path.Combine(Application.persistentDataPath, filename + TEMP_PREFIX);
		}

		public bool Exists() => File.Exists(_path);

		public IDisposable Save(byte[] obj, Action onComplete)
		{
			return Scheduler.ThreadPool.Schedule(() =>
			{
				if (Exists())
				{
					FileUtils.DeleteFile(_pathBackup);
					File.Move(_path, _pathBackup);
				}

				FileUtils.CreateDirectoryAndWriteAllBytes(_pathTemp, obj);
				File.Move(_pathTemp, _path);
				Scheduler.MainThread.Schedule(onComplete);
			});
		}

		public byte[] Load()
		{
			if (!Exists())
			{
				if (File.Exists(_pathBackup))
					return File.ReadAllBytes(_pathBackup);
			}
			else
			{
				return File.ReadAllBytes(_path);
			}

			return null;
		}

		public void Remove() => FileUtils.DeleteFile(_path);
	}
}