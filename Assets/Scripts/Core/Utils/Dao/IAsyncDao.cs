﻿using System;

namespace Core.Utils.Dao
{
	public interface IAsyncDao<T>
	{
		bool Exists();
		IDisposable Save(T obj, Action onComplete);
		T Load();
		void Remove();
	}
}