using System.IO;

namespace Core.Utils.Dao
{
	public static class FileUtils
	{
		/// <summary>
		///     Create directory if doesnt exist and write all text
		/// </summary>
		public static bool CreateDirectoryAndWriteAllText(string path, string text)
		{
			if (!CreateDirectoryForFile(path))
				return false;

			File.WriteAllText(path, text);
			return true;
		}

		/// <summary>
		///     Create directory if doesnt exist and write all bytes
		/// </summary>
		public static bool CreateDirectoryAndWriteAllBytes(string path, byte[] bytes)
		{
			if (!CreateDirectoryForFile(path))
				return false;

			File.WriteAllBytes(path, bytes);
			return true;
		}

		public static bool DeleteFile(string path)
		{
			if (!File.Exists(path))
				return false;

			File.Delete(path);
			return true;
		}

		private static bool CreateDirectoryForFile(string path)
		{
			var fileInfo = new FileInfo(path);
			var directory = fileInfo.Directory;
			if (directory == null)
				return false;

			if (!directory.Exists)
				directory.Create();
			return true;
		}
	}
}