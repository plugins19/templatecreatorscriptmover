namespace Core.Utils.Dao
{
	public interface IDao<T>
	{
		bool Exists();

		void Save(T obj);

		T Load(T defaultValue = default);

		void Remove();
	}
}