﻿namespace Core.Utils.Dao
{
	public interface IBytesStorageAsyncDao : IAsyncDao<byte[]>
	{
	}
}