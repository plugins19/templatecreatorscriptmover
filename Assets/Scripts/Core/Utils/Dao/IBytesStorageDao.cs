﻿namespace Core.Utils.Dao
{
	public interface IBytesStorageDao : IDao<byte[]>
	{
	}
}