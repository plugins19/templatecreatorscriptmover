using UnityEngine;

namespace Core.Utils.Screens
{
	public interface IScreenUtils
	{
		Vector2 Size { get; }
		
		int Width { get; }
		
		int Height { get; }
	}
}