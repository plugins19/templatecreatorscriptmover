using UnityEngine;

namespace Core.Utils.Screens.Impls
{
	public class ScreenUtils : IScreenUtils
	{
		public Vector2 Size => new(Screen.width, Screen.height);

		public int Width => Screen.width;

		public int Height => Screen.height;
	}
}