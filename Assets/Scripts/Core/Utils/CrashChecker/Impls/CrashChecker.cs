using System;
using UnityEngine;
using Zenject;

namespace Core.Utils.CrashChecker.Impls
{
	public class CrashChecker : ICrashChecker, IInitializable, IDisposable
	{
		public bool HasExceptions { get; private set; }
		
		public void Initialize() => Application.logMessageReceived += OnLogMessageReceived;

		public void Dispose() => Application.logMessageReceived -= OnLogMessageReceived;

		private void OnLogMessageReceived(string condition, string stackTrace, LogType type)
		{
			if (type == LogType.Exception)
				HasExceptions = true;
		}
	}
}