namespace Core.Utils.CrashChecker
{
	public interface ICrashChecker
	{
		bool HasExceptions { get; }
	}
}