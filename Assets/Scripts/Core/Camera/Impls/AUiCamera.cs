using UnityEngine;

namespace Core.Camera.Impls
{
    public abstract class AUiCamera : MonoBehaviour, IUiCamera
    {
        [SerializeField] private UnityEngine.Camera uiCamera;

        public UnityEngine.Camera Camera => uiCamera;

        public Vector3 WorldToViewportPoint(Vector3 position) => uiCamera.WorldToViewportPoint(position);

        public Vector3 WorldToScreenPoint(Vector3 position) => uiCamera.WorldToScreenPoint(position);
        
        public Vector3 ViewportToWorldPoint(Vector3 position) => uiCamera.ViewportToWorldPoint(position);

        public Vector3 ViewportToScreenPoint(Vector3 position) => uiCamera.ViewportToScreenPoint(position);
        
        public Vector3 ScreenToWorldPoint(Vector3 position) => uiCamera.ScreenToWorldPoint(position);

        public Vector3 ScreenToViewportPoint(Vector3 position) => uiCamera.ScreenToViewportPoint(position);
    }
}