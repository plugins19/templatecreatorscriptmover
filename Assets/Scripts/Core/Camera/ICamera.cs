using UnityEngine;

namespace Core.Camera
{
    public interface ICamera
    {
        UnityEngine.Camera Camera { get; }
        
        Vector3 WorldToViewportPoint(Vector3 position);
        
        Vector3 WorldToScreenPoint(Vector3 position);
        
        Vector3 ViewportToWorldPoint(Vector3 position);
        
        Vector3 ViewportToScreenPoint(Vector3 position);

        Vector3 ScreenToWorldPoint(Vector3 position);

        Vector3 ScreenToViewportPoint(Vector3 position);
    }
}