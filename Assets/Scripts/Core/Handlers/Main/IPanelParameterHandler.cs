using SimpleUi.Signals;

namespace Core.Handlers.Main
{
    public interface IPanelParameterHandler<TView, TParameter>
    {
        TView View { get; }
        
        TParameter Parameter { get; }
        
        void Initialize(TView view, TParameter parameter, EWindowLayer windowLayer = EWindowLayer.Local);
    }
}