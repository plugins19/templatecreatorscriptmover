using SimpleUi.Signals;

namespace Core.Handlers.Main
{
	public interface IPanelHandler<TView>
	{
		TView View { get; }

		void Initialize(TView view, EWindowLayer windowLayer = EWindowLayer.Local);
	}
}