using SimpleUi.Signals;

namespace Core.Handlers.Main.Impls
{
	public abstract class APanelHandler<TView> : IPanelHandler<TView>
	{
		public TView View { get; private set; }

		protected EWindowLayer WindowLayer;

		public virtual void Initialize(TView view, EWindowLayer windowLayer = EWindowLayer.Local)
		{
			View = view;
			WindowLayer = windowLayer;
		}
	}
}