using SimpleUi.Signals;

namespace Core.Handlers.Main.Impls
{
    public abstract class APanelParameterHandler<TView, TParameter> : IPanelParameterHandler<TView, TParameter>
    {
        public TView View { get; private set; }
        public TParameter Parameter { get; private set; }

        protected EWindowLayer WindowLayer;
        
        public virtual void Initialize(TView view, TParameter parameter, EWindowLayer windowLayer = EWindowLayer.Local)
        {
            View = view;
            Parameter = parameter;
            WindowLayer = windowLayer;
        }
    }
}