namespace Db.Builds
{
	public interface IBuildSetting
	{
		EBuildType BuildType { get; }
	}
}