using UnityEngine;

namespace Db.Builds.Impls
{
	[CreateAssetMenu(menuName = "Databases/BUild/" + nameof(BuildSetting), fileName = nameof(BuildSetting), order = 0)]
	public class BuildSetting : ScriptableObject, IBuildSetting
	{
		[SerializeField] private EBuildType buildType = EBuildType.Debug;

		public EBuildType BuildType => buildType;
	}
}