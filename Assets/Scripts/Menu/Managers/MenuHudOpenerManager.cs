using Core.Managers;
using Ui.Menu.Windows;
using Zenject;

namespace Menu.Managers
{
    public class MenuHudOpenerManager : AHudOpenerManager<MainMenuWindow>
    {
        public MenuHudOpenerManager(
            SignalBus signalBus
        ) : base(signalBus)
        {
        }
    }
}