using Core.Services.Scenes;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace ForRemoving
{
	public class CreateScenesTool
	{
		[MenuItem("Tools/CreateTemplate/7: Create scenes", false, 7)]
		private static void CreateResources()
		{
			CreateScene(ESceneType.Splash);
			CreateScene(ESceneType.Menu);
			CreateScene(ESceneType.Game);
			var sampleScenePath = $"Assets/Scenes/SampleScene.unity";
			AssetDatabase.DeleteAsset(sampleScenePath);
			AssetDatabase.Refresh();
			Debug.Log("Scenes created");
		}

		private static void CreateScene(ESceneType type)
		{
			var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
			newScene.name = type.ToString();
			var scenePath = $"Assets/Scenes/{newScene.name}.unity";
			EditorSceneManager.SaveScene(newScene, scenePath);
			var editorBuildSettingsScenes = EditorBuildSettings.scenes;
			var editorBuildSettingsScene = new EditorBuildSettingsScene(scenePath, true);
			ArrayUtility.Add(ref editorBuildSettingsScenes, editorBuildSettingsScene);
			EditorBuildSettings.scenes = editorBuildSettingsScenes;
		}
	}
}