using System.Collections.Generic;
using System.Reflection;
using AttaboysGames.Runtime.Installers;
using AttaboysGames.TemplateCreatorPackageImporter.Editor;
using Core.Runtime.Settings.Impls;
using Core.Services.Scenes;
using Installers.Game;
using Installers.Menu;
using Installers.Project;
using Installers.Splash;
using SimpleUi;
using SimpleUi.Signals;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace ForRemoving
{
	public class ContextCreatorTool
	{
		[MenuItem("Tools/CreateTemplate/8: Create contexts", false, 8)]
		private static void CreateContexts()
		{
			CreateSceneContext<SplashInstaller, SplashUiInstaller>(ESceneType.Splash);
			CreateSceneContext<MenuInstaller, MenuUiInstaller>(ESceneType.Menu);
			CreateSceneContext<GameInstaller, GameUiInstaller>(ESceneType.Game);
			CreateProjectContext();
			Debug.Log("Created contexts");
		}

		private static void CreateSceneContext<TInstaller, TUiInstaller>(
			ESceneType sceneType
		)
			where TInstaller : ScriptableObjectInstaller
			where TUiInstaller : ScriptableObjectInstaller
		{
			var sceneName = sceneType.ToString();
			EditorSceneManager.OpenScene($"Assets/Scenes/{sceneName}.unity");
			var contextGameObject = new GameObject($"{sceneName}Context");
			var sceneContextComponent = contextGameObject.AddComponent<SceneContext>();
			var simpleUiInstallerComponent = contextGameObject.AddComponent<SimpleUiInstaller>();
			var installerByName = TemplateCreatorToolHelper.GetNameByType<TInstaller>();
			var sceneInstaller = AssetDatabase.LoadAssetAtPath<TInstaller>(
				$"Assets/Resources/Installers/{sceneName}/{installerByName}.asset");
			var uiInstallerByName = TemplateCreatorToolHelper.GetNameByType<TUiInstaller>();
			var sceneUiInstaller = AssetDatabase.LoadAssetAtPath<TUiInstaller>(
				$"Assets/Resources/Installers/{sceneName}/{uiInstallerByName}.asset");
			var scriptableObjectInstallers = new List<ScriptableObjectInstaller>
			{
				sceneInstaller,
				sceneUiInstaller
			};
			sceneContextComponent.ScriptableObjectInstallers = scriptableObjectInstallers;
			var monoInstallers = new List<MonoInstaller>()
			{
				simpleUiInstallerComponent,
			};
			sceneContextComponent.Installers = monoInstallers;
			var isAutoRun = sceneType == ESceneType.Splash;
			                var autoRunField = sceneContextComponent.GetType().BaseType
				.GetField("_autoRun", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			autoRunField.SetValue(sceneContextComponent, isAutoRun);
			var activeScene = SceneManager.GetActiveScene();
			EditorSceneManager.SaveScene(activeScene);
		}

		private static void CreateProjectContext()
		{
			var gameObject = new GameObject();
			var projectContext = gameObject.AddComponent<ProjectContext>();
			var simpleUiInstaller = gameObject.AddComponent<SimpleUiInstaller>();
			var adInstaller = gameObject.AddComponent<AdInstaller>();
			var fieldInfo = simpleUiInstaller.GetType().GetField("windowLayer",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			fieldInfo.SetValue(simpleUiInstaller, EWindowLayer.Project);
			var adMobSettingField = adInstaller.GetType().GetField("adMobSettingsDatabase",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			var adMobSettingsDatabase = AssetDatabase.LoadAssetAtPath<AdMobSettingsDatabase>(
				"Assets/Resources/Settings/AdMobSettingsDatabase.asset");
			adMobSettingField.SetValue(adInstaller, adMobSettingsDatabase);
			var monoInstallers = new List<MonoInstaller>()
			{
				simpleUiInstaller,
				adInstaller
			};
			projectContext.Installers = monoInstallers;
			var installerByName = TemplateCreatorToolHelper.GetNameByType<ProjectInstaller>();
			var installer = AssetDatabase.LoadAssetAtPath<ProjectInstaller>(
				$"Assets/Resources/Installers/Project/{installerByName}.asset");
			var uiInstallerByName = TemplateCreatorToolHelper.GetNameByType<ProjectUiInstaller>();
			var uiInstaller = AssetDatabase.LoadAssetAtPath<ProjectUiInstaller>(
				$"Assets/Resources/Installers/Project/{uiInstallerByName}.asset");
			var scriptableObjectInstallers = new List<ScriptableObjectInstaller>()
			{
				installer,
				uiInstaller
			};
			projectContext.ScriptableObjectInstallers = scriptableObjectInstallers;
			PrefabUtility.SaveAsPrefabAsset(gameObject, "Assets/Resources/ProjectContext.prefab");
			Object.DestroyImmediate(gameObject);
		}
	}
}