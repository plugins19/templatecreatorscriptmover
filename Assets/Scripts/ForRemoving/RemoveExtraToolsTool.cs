using UnityEditor;
using UnityEngine;

namespace ForRemoving
{
	public class RemoveExtraToolsTool
	{
		[MenuItem("Tools/CreateTemplate/10: Remove extra tools", false, 10)]
		private static void RemoveExtraPackages()
		{
			AssetDatabase.DeleteAsset("Assets/Scripts/ForRemoving");
			Debug.Log("All extra tools was removed");
		}
	}
}