using System.IO;
using System.Reflection;
using AttaboysGames.TemplateCreatorPackageImporter.Editor;
using Core.Camera.Impls;
using SimpleUi.Abstracts;
using Ui.Game.Main;
using Ui.Menu.Main;
using Ui.Project.Loading;
using Ui.Splash;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ForRemoving
{
	public class CreatePrefabsTool
	
	{
		[MenuItem("Tools/CreateTemplate/5: Create prefabs folder", false, 5)]
		private static void CreateResourceFolder()
		{
			var prefabFolderPath = TemplateCreatorToolHelper.CreateFolder("Assets", "Prefabs");
			var uiPrefabsPath = TemplateCreatorToolHelper.CreateFolder(prefabFolderPath, "Ui");
			CreateCanvas<SceneUiCamera>(uiPrefabsPath, "Canvas", 9f);
			CreateCanvas<ProjectUiCamera>(uiPrefabsPath, "ProjectCanvas", 10f);
			var splashFolder = TemplateCreatorToolHelper.CreateFolder(uiPrefabsPath, "Splash");
			var menuFolder = TemplateCreatorToolHelper.CreateFolder(uiPrefabsPath, "Menu");
			var gameFolder = TemplateCreatorToolHelper.CreateFolder(uiPrefabsPath, "Game");
			var projectFolder = TemplateCreatorToolHelper.CreateFolder(uiPrefabsPath, "Project");
			CreateUiView<SplashView>(splashFolder);
			CreateUiView<MainMenuView>(menuFolder);
			CreateUiView<MainGameView>(gameFolder);
			CreateUiView<LoadingView>(projectFolder);
			CreateEventSystem(uiPrefabsPath);
		}

		private static void CreateCanvas<TUiCamera>(
			string rootPath,
			string canvasName,
			float depth
		)
			where TUiCamera : AUiCamera
		{
			var canvas = new GameObject(canvasName)
			{
				layer = 5
			};
			var canvasComponent = canvas.AddComponent<Canvas>();
			var cameraObject = new GameObject(TemplateCreatorToolHelper.GetNameByType<TUiCamera>());
			cameraObject.transform.SetParent(canvas.transform);
			var cameraComponent = cameraObject.AddComponent<Camera>();
			cameraComponent.clearFlags = CameraClearFlags.Depth;
			cameraComponent.cullingMask = 32;
			cameraComponent.orthographic = true;
			cameraComponent.orthographicSize = 5;
			cameraComponent.farClipPlane = 0.1f;
			cameraComponent.nearClipPlane = 0f;
			cameraComponent.depth = depth;
			cameraObject.AddComponent<AudioListener>();
			var uiCameraComponent = cameraObject.AddComponent<TUiCamera>();
			var cameraField = uiCameraComponent.GetType().BaseType.GetField("uiCamera",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			cameraField.SetValue(uiCameraComponent, cameraComponent);
			canvasComponent.renderMode = RenderMode.ScreenSpaceCamera;
			canvasComponent.worldCamera = cameraComponent;
			var canvasScalerComponent = canvas.AddComponent<CanvasScaler>();
			canvasScalerComponent.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			canvasScalerComponent.referenceResolution = new Vector2(1080, 1920);
			canvasScalerComponent.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
			canvasScalerComponent.matchWidthOrHeight = 0f;
			canvas.AddComponent<GraphicRaycaster>();
			var finalPath = Path.Combine(rootPath, $"{canvasName}.prefab");
			PrefabUtility.SaveAsPrefabAsset(canvas, finalPath);
		}
		
		private static void CreateUiView<TUiView>(string prefabPath)
			where TUiView : UiView
		{
			var canvas = GameObject.FindObjectOfType<Canvas>();
			var nameByType = TemplateCreatorToolHelper.GetNameByType<TUiView>();
			var uiView = new GameObject(nameByType);
			var canvasTransform = canvas.transform as RectTransform;
			uiView.transform.SetParent(canvasTransform, false);
			var uiViewTransform = uiView.AddComponent<RectTransform>();
			uiView.AddComponent<CanvasRenderer>();
			uiViewTransform.anchorMin = Vector2.zero;
			uiViewTransform.anchorMax = Vector2.one;
			uiViewTransform.sizeDelta = Vector2.zero;
			uiViewTransform.pivot = Vector2.one * 0.5f;
			uiView.AddComponent<TUiView>();
			var finalPath = Path.Combine(prefabPath, $"{nameByType}.prefab");
			PrefabUtility.SaveAsPrefabAsset(uiView, finalPath);
		}

		private static void CreateEventSystem(string rootPath)
		{
			var eventSystem = new GameObject("EventSystem");
			eventSystem.AddComponent<EventSystem>();
			eventSystem.AddComponent<StandaloneInputModule>();
			var finalPath = Path.Combine(rootPath, $"EventSystem.prefab");
			PrefabUtility.SaveAsPrefabAsset(eventSystem, finalPath);

		}
	}
}