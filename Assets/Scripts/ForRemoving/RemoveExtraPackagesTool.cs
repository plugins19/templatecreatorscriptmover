using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;

namespace ForRemoving
{
	public class RemoveExtraPackagesTool
	{
		private static readonly string[] packageUrls = {
			"com.unity.modules.xr",
			"com.unity.modules.wind",
			"com.unity.modules.vr",
			"com.unity.modules.terrainphysics",
			"com.unity.modules.terrain",
			"com.unity.modules.cloth",
			"com.unity.visualscripting",
			"https://gitlab.com/plugins19/templatecreatorpackageimporter.git#upm",
			"https://gitlab.com/plugins19/templatecreatorscriptmover.git#upm",
		};
		
		private static RemoveRequest _request;
		private static int _currentPackageUrlIndex;
		
		[MenuItem("Tools/CreateTemplate/9: Remove extra packages", false, 9)]
		private static void RemoveExtraPackages()
		{
			_currentPackageUrlIndex = 0;
			RemoveNextPackage();
		}

		private static void RemoveNextPackage()
		{
			RemovePackage(packageUrls[_currentPackageUrlIndex]);
		}

		private static void RemovePackage(string packageUrl)
		{
			_request = Client.Remove(packageUrl);
			EditorApplication.update += PackageAddingProcess;
		}

		private static void PackageAddingProcess()
		{
			if(!_request.IsCompleted)
				return;
			
			switch (_request.Status)
			{
				case StatusCode.Success:
					Debug.Log($"Removed: {_request.PackageIdOrName}");
					break;
				case StatusCode.Failure:
					Debug.Log(_request.Error.message);
					break;
			}

			EditorApplication.update -= PackageAddingProcess;
			_currentPackageUrlIndex++;

			if (_currentPackageUrlIndex == packageUrls.Length)
			{
				Debug.Log($"All packages removed");
				return;
			}
			
			RemoveNextPackage();
		}
	}
}