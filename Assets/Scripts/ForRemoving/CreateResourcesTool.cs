using System.IO;
using System.Reflection;
using AttaboysGames.TemplateCreatorPackageImporter.Editor;
using Core.Runtime.Settings.Impls;
using Core.Services.Scenes;
using Db.Builds.Impls;
using Installers.Game;
using Installers.Menu;
using Installers.Project;
using Installers.Splash;
using SimpleUi.Abstracts;
using Ui.Game.Main;
using Ui.Menu.Main;
using Ui.Project.Loading;
using Ui.Splash;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace ForRemoving
{
	public class CreateResourcesTool
	{
		[MenuItem("Tools/CreateTemplate/6: Create resources folder", false, 6)]
		private static void CreateResources()
		{
			var resourcesPath = TemplateCreatorToolHelper.CreateFolder("Assets", "Resources");
			var settingsPath = TemplateCreatorToolHelper.CreateFolder(resourcesPath, "Settings");
			var installersPath = TemplateCreatorToolHelper.CreateFolder(resourcesPath, "Installers");
			CreateAsset<BuildSetting>(settingsPath);
			CreateAsset<AdMobSettingsDatabase>(settingsPath);
			CreateSceneInstallers<SplashInstaller, SplashUiInstaller, SplashView>(installersPath,
				ESceneType.Splash.ToString());
			CreateSceneInstallers<MenuInstaller, MenuUiInstaller, MainMenuView>(installersPath,
				ESceneType.Menu.ToString());
			CreateSceneInstallers<GameInstaller, GameUiInstaller, MainGameView>(installersPath,
				ESceneType.Game.ToString());
			CreateProjectInstallers<ProjectInstaller, ProjectUiInstaller, LoadingView>(installersPath, "Project");
			Debug.Log("Resources created");
		}

		private static void CreateSceneInstallers<TInstaller, TUiInstaller, TUiView>(
			string rootPath,
			string folderName
		)
			where TInstaller : ScriptableObjectInstaller
			where TUiInstaller : ScriptableObject
			where TUiView : UiView
		{
			var installersFolder = TemplateCreatorToolHelper.CreateFolder(rootPath, folderName);
			CreateAsset<TInstaller>(installersFolder);
			CreateAsset<TUiInstaller>(installersFolder);
			var canvas = AssetDatabase.LoadAssetAtPath<Canvas>("Assets/Prefabs/Ui/Canvas.prefab");
			var eventSystem = AssetDatabase.LoadAssetAtPath<EventSystem>("Assets/Prefabs/Ui/EventSystem.prefab");
			var viewName = TemplateCreatorToolHelper.GetNameByType<TUiView>();
			var view = AssetDatabase.LoadAssetAtPath<TUiView>($"Assets/Prefabs/Ui/{folderName}/{viewName}.prefab");
			var installerName = TemplateCreatorToolHelper.GetNameByType<TUiInstaller>();
			var installer = AssetDatabase.LoadAssetAtPath<TUiInstaller>(
				$"Assets/Resources/Installers/{folderName}/{installerName}.asset");
			var installerType = installer.GetType();
			var viewFieldName = char.ToLower(viewName[0]) + viewName.Substring(1);
			var viewFieldInfo = installerType.GetField(viewFieldName,
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			viewFieldInfo.SetValue(installer, view);
			var sceneUiInstallerType = installerType.BaseType;
			var eventSystemField = sceneUiInstallerType.GetField("eventSystem",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			eventSystemField.SetValue(installer, eventSystem);
			var uiInstallerType = sceneUiInstallerType.BaseType;
			var canvasField = uiInstallerType.GetField("canvas",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			canvasField.SetValue(installer, canvas);
		}

		private static void CreateProjectInstallers<TInstaller, TUiInstaller, TUiView>(
			string rootPath,
			string folderName
		)
			where TInstaller : ScriptableObjectInstaller
			where TUiInstaller : ScriptableObject
			where TUiView : UiView
		{
			var installersFolder = TemplateCreatorToolHelper.CreateFolder(rootPath, folderName);
			CreateAsset<TInstaller>(installersFolder);
			CreateAsset<TUiInstaller>(installersFolder);
			var canvas = AssetDatabase.LoadAssetAtPath<Canvas>("Assets/Prefabs/Ui/Canvas.prefab");
			var viewName = TemplateCreatorToolHelper.GetNameByType<TUiView>();
			var view = AssetDatabase.LoadAssetAtPath<TUiView>($"Assets/Prefabs/Ui/{folderName}/{viewName}.prefab");
			var installerName = TemplateCreatorToolHelper.GetNameByType<TUiInstaller>();
			var installer = AssetDatabase.LoadAssetAtPath<TUiInstaller>(
				$"Assets/Resources/Installers/{folderName}/{installerName}.asset");
			var installerType = installer.GetType();
			var viewFieldName = char.ToLower(viewName[0]) + viewName.Substring(1);
			var viewFieldInfo = installerType.GetField(viewFieldName,
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			viewFieldInfo.SetValue(installer, view);
			var uiInstallerType = installerType.BaseType;
			var canvasField = uiInstallerType.GetField("canvas",
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			canvasField.SetValue(installer, canvas);
		}

		private static void CreateAsset<TAsset>(string rootPath)
			where TAsset : ScriptableObject
		{
			var database = ScriptableObject.CreateInstance<TAsset>();
			var nameByType = TemplateCreatorToolHelper.GetNameByType<TAsset>();
			var finalPath = Path.Combine(rootPath, $"{nameByType}.asset");
			AssetDatabase.CreateAsset(database, finalPath);
			AssetDatabase.SaveAssets();
		}
	}
}