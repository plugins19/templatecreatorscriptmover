using Core.Managers;
using Ui.Game.Windows;
using Zenject;

namespace Game.Managers.Impls
{
	public class GameWindowManager : AHudOpenerManager<GameHudWindow>
	{
		public GameWindowManager(
			SignalBus signalBus
		) : base(signalBus)
		{
		}
	}
}