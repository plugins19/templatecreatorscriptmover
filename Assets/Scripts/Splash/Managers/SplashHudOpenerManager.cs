using Core.Managers;
using Ui.Splash;
using Zenject;

namespace Splash.Managers
{
    public class SplashHudOpenerManager : AHudOpenerManager<SplashWindow>
    {
        public SplashHudOpenerManager(
            SignalBus signalBus
        ) : base(signalBus)
        {
        }
    }
}